

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for f_action
-- ----------------------------
DROP TABLE IF EXISTS `f_action`;
CREATE TABLE `f_action` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `act_icon` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `act_pid` int(11) DEFAULT '0' COMMENT '父类id',
  `act_name` varchar(26) COLLATE utf8_unicode_ci NOT NULL COMMENT '名称',
  `act_group` varchar(12) COLLATE utf8_unicode_ci NOT NULL COMMENT '模块名',
  `act_action` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '控制器',
  `act_type` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2' COMMENT '1：开启 2：关闭 3:已删除',
  `is_display` char(2) COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1:显示 2：隐藏',
  PRIMARY KEY (`act_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_action
-- ----------------------------
INSERT INTO `f_action` VALUES ('2', '&#xe620;', '0', '系统管理', 'Admin', '', '1', '1');
INSERT INTO `f_action` VALUES ('3', '&#xe631;', '0', '站点管理', 'Admin', '', '1', '1');
INSERT INTO `f_action` VALUES ('4', '', '3', '站点列表', 'Admin', 'Site/lists', '1', '1');
INSERT INTO `f_action` VALUES ('5', '', '3', '站点脚本', 'Admin', 'Site/pythonShell', '1', '1');
INSERT INTO `f_action` VALUES ('6', '', '3', '站点商品', 'Admin', 'Task/lists', '1', '1');
INSERT INTO `f_action` VALUES ('7', '&#xe613;', '0', '团队管理', 'Admin', '', '1', '1');
INSERT INTO `f_action` VALUES ('8', '', '7', '团队列表', 'Admin', 'Team/memberlist', '1', '1');
INSERT INTO `f_action` VALUES ('10', '', '7', '所有团队账号', 'Admin', 'Team/accountlist', '1', '1');
INSERT INTO `f_action` VALUES ('11', '', '2', '权限列表', 'Admin', 'System/jurisdiction', '1', '1');
INSERT INTO `f_action` VALUES ('12', '', '2', '角色管理', 'Admin', 'System/roleJurisdiction', '1', '1');
INSERT INTO `f_action` VALUES ('13', '', '2', '系统设置', 'Admin', 'System/setup', '1', '1');
INSERT INTO `f_action` VALUES ('14', '', '2', '修改密码', 'Admin', 'System/updateinfo', '1', '2');
INSERT INTO `f_action` VALUES ('15', '', '11', '添加权限', 'Admin', 'System/jurisdictionAdd', '1', '2');
INSERT INTO `f_action` VALUES ('16', '', '11', '编辑权限', 'Admin', 'System/jurisdictionEdit', '1', '2');
INSERT INTO `f_action` VALUES ('17', '', '12', '添加角色', 'Admin', 'System/roleAdd', '1', '2');
INSERT INTO `f_action` VALUES ('18', '', '12', '权限查看分配', 'Admin', 'System/ruleGroup', '1', '2');
INSERT INTO `f_action` VALUES ('19', '', '11', '删除权限', 'Admin', 'System/jurisdictionDelete', '1', '2');
INSERT INTO `f_action` VALUES ('20', '', '8', '添加团队', 'Admin', 'Team/add', '1', '1');
INSERT INTO `f_action` VALUES ('21', '&#xe629;', '0', '任务管理', 'Admin', '', '1', '1');
INSERT INTO `f_action` VALUES ('23', '', '10', '随机批量假账号', 'Admin', 'Team/randomImport', '1', '1');
INSERT INTO `f_action` VALUES ('24', '', '10', '提交注册任务', 'Admin', 'Team/register', '1', '1');
INSERT INTO `f_action` VALUES ('27', '', '6', '添加商品', 'Admin', 'Task/addTask', '1', '1');
INSERT INTO `f_action` VALUES ('28', '', '6', '商品批量处理', 'Admin', 'Task/batch', '1', '1');
INSERT INTO `f_action` VALUES ('29', '', '6', '删除商品', 'Admin', 'Task/deletetask', '1', '1');
INSERT INTO `f_action` VALUES ('32', '', '7', '抢购账号', 'Admin', 'MyTeam/accountPanic', '1', '1');
INSERT INTO `f_action` VALUES ('33', '', '32', 'excel导入账号', 'Admin', 'MyTeam/accountExcelImport', '1', '1');
INSERT INTO `f_action` VALUES ('34', '', '32', '获取当前用户定位', 'Admin', 'MyTeam/baidumap', '1', '1');
INSERT INTO `f_action` VALUES ('35', '', '32', '注册第三方平台', 'Admin', 'MyTeam/register', '1', '1');
INSERT INTO `f_action` VALUES ('36', '', '10', '获取当前用户定位', 'Admin', 'Team/baidumap', '1', '1');
INSERT INTO `f_action` VALUES ('37', '&#xe63c;', '0', '订单管理', 'Admin', '', '1', '1');
INSERT INTO `f_action` VALUES ('38', '', '37', '团队订单', 'Admin', 'MyOrder/lists', '1', '1');
INSERT INTO `f_action` VALUES ('40', '', '38', '支付', 'Admin', 'MyOrder/goPay', '1', '1');
INSERT INTO `f_action` VALUES ('41', '', '38', '支付确认', 'Admin', 'MyOrder/confirmGoPay', '1', '1');
INSERT INTO `f_action` VALUES ('42', '', '38', '获取快递单号', 'Admin', 'MyOrder/getExpress', '1', '1');
INSERT INTO `f_action` VALUES ('43', '', '7', '团队成员', 'Admin', 'MyTeam/staff', '1', '1');
INSERT INTO `f_action` VALUES ('44', '', '43', '添加成员', 'Admin', 'MyTeam/staffAdd', '1', '1');
INSERT INTO `f_action` VALUES ('45', '&#xe628;', '0', '服务器管理', 'Admin', '', '1', '1');
INSERT INTO `f_action` VALUES ('46', '', '45', '服务平台配置', 'Admin', 'Service/serviceConf', '1', '1');
INSERT INTO `f_action` VALUES ('47', '', '45', '服务平台', 'Admin', 'MyService/platfrom', '1', '1');
INSERT INTO `f_action` VALUES ('48', '', '21', '所有任务', 'Admin', 'Team/teamTask', '1', '1');
INSERT INTO `f_action` VALUES ('49', '', '48', '创建团队任务', 'Admin', 'Team/createTeamTask', '1', '1');
INSERT INTO `f_action` VALUES ('50', '', '21', '任务列表', 'Admin', 'MyTeam/myTask', '1', '1');
INSERT INTO `f_action` VALUES ('51', '', '50', '创建任务', 'Admin', 'MyTeam/myCreateTask', '1', '1');
INSERT INTO `f_action` VALUES ('52', '', '50', '删除任务', 'Admin', 'MyTeam/deletaskrel', '1', '1');
INSERT INTO `f_action` VALUES ('53', '', '50', '创建匹配任务', 'Admin', 'MyTeam/searchtask', '1', '1');
INSERT INTO `f_action` VALUES ('54', '', '50', '终止任务', 'Admin', 'MyTeam/stopTaskrel', '1', '1');
INSERT INTO `f_action` VALUES ('55', '', '37', '已支付订单', 'Admin', 'MyOrder/paid', '1', '1');
INSERT INTO `f_action` VALUES ('56', '', '37', '所有订单', 'Admin', 'AllOrder/allOrder', '1', '1');
INSERT INTO `f_action` VALUES ('57', '', '21', '执行中任务', 'Admin', 'MyTeam/inExecution', '1', '1');
INSERT INTO `f_action` VALUES ('58', '', '21', '已结束任务', 'Admin', 'MyTeam/endTaskrel', '1', '1');
INSERT INTO `f_action` VALUES ('59', '', '45', '镜像列表', 'Admin', 'MyService/imgTable', '1', '1');
INSERT INTO `f_action` VALUES ('60', '', '59', '添加修改镜像', 'Admin', 'MyService/imgInsert', '1', '1');
INSERT INTO `f_action` VALUES ('61', '', '59', '删除镜像', 'Admin', 'MyService/delImg', '1', '1');
INSERT INTO `f_action` VALUES ('62', '', '45', '主机管理', 'Admin', 'MyService/hoslist', '1', '1');
INSERT INTO `f_action` VALUES ('63', '', '62', '创建主机', 'Admin', 'MyService/hostadd', '1', '1');
INSERT INTO `f_action` VALUES ('64', '', '45', '平台套餐', 'Admin', 'Service/platfromPakeage', '1', '1');
INSERT INTO `f_action` VALUES ('65', '', '64', '新增平台套餐模板', 'Admin', 'Service/platfromPakeageadd', '1', '1');
INSERT INTO `f_action` VALUES ('66', '', '64', '新增平台套餐', 'Admin', 'Service/platfromPakeageaddto', '1', '1');
INSERT INTO `f_action` VALUES ('67', '', '59', '获取镜像', 'Admin', 'MyService/imgGetlow', '1', '1');
INSERT INTO `f_action` VALUES ('68', null, null, '', '', '', '3', '1');
INSERT INTO `f_action` VALUES ('69', '', '47', '添加服务平台', 'Admin', 'MyService/platfromadd', '1', '1');
INSERT INTO `f_action` VALUES ('70', '', '62', '批量删除', 'Admin', 'MyService/batchdel', '1', '1');
INSERT INTO `f_action` VALUES ('71', '', '62', '删除单个主机', 'Admin', 'MyService/onedel', '1', '1');
INSERT INTO `f_action` VALUES ('72', '', '64', '平台主机规格列表', 'Admin', 'Service/platfromDescribeInstanceTypes', '1', '1');
INSERT INTO `f_action` VALUES ('73', '', '59', '获取镜像地域列表', 'Admin', 'MyService/discribeRegions', '1', '1');
INSERT INTO `f_action` VALUES ('74', '', '4', '添加编辑站点', 'Admin', 'Site/addSite', '1', '1');
INSERT INTO `f_action` VALUES ('75', '', '4', '删除站点', 'Admin', 'Site/deleteSite', '1', '1');
INSERT INTO `f_action` VALUES ('76', '', '32', '注册任务', 'Admin', 'MyTeam/accountRegister', '1', '1');

-- ----------------------------
-- Table structure for f_group
-- ----------------------------
DROP TABLE IF EXISTS `f_group`;
CREATE TABLE `f_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_enable` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1:正常 2：已删除',
  `group_val` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_group
-- ----------------------------
INSERT INTO `f_group` VALUES ('1', '超级管理员', '0', '1', '2,11,15,16,19,12,17,18,13,14,3,4,6,27,28,29,7,8,20,10,23,24,36,32,33,34,35,76,43,44,21,48,49,50,51,52,53,54,57,58,37,38,40,41,42,55,56,45,46,47,69,59,60,61,67,73,62,63,70,71,64,65,66,72');
INSERT INTO `f_group` VALUES ('2', '团队管理员', '0', '1', '2,13,14,7,32,33,34,35,76,43,44,21,50,51,52,53,54,57,58,37,38,40,41,42,55,45,47,69,59,60,61,67,73,62,63,70,71');
INSERT INTO `f_group` VALUES ('3', '团队成员', '0', '1', '37,38,40,41,42,55');

-- ----------------------------
-- Table structure for f_img
-- ----------------------------
DROP TABLE IF EXISTS `f_img`;
CREATE TABLE `f_img` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_id` int(11) DEFAULT NULL,
  `img_name` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_type` char(2) COLLATE utf8_unicode_ci NOT NULL COMMENT '1:美团云，2：腾讯云 ,3：ucloud,4:阿里云',
  `img_url` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_status` char(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1:开启 2：关闭',
  `img_regions` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_img
-- ----------------------------
INSERT INTO `f_img` VALUES ('1', '1', '测试', '4', 'm-m5e34o8c50ub0sbrwcp5', '1', 'cn-qingdao:青岛');
INSERT INTO `f_img` VALUES ('2', '2', 'windows-templet', '1', 'fbe89998-0a17-43a2-aa49-1f79b818ca1a', '1', 'suoyou:所有');
INSERT INTO `f_img` VALUES ('3', '2', 'hb-A', '1', '809fdec1-de16-4976-8dfd-af18f9300c71', '1', 'suoyou:所有');
INSERT INTO `f_img` VALUES ('4', '2', 'ceshijingxiang', '2', 'img-idzgi2pz', '1', 'ap-guangzhou:广州');
INSERT INTO `f_img` VALUES ('5', '2', '测试', '4', 'm-m5e34o8c50ub0sbrwcp5', '1', 'cn-qingdao:青岛');

-- ----------------------------
-- Table structure for f_member
-- ----------------------------
DROP TABLE IF EXISTS `f_member`;
CREATE TABLE `f_member` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(26) COLLATE utf8_unicode_ci NOT NULL,
  `m_email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `m_pwd` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `m_status` char(2) COLLATE utf8_unicode_ci DEFAULT '2' COMMENT '1:活跃2：关闭',
  `m_type` char(2) COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '0:超级管理员 1:团队管理员 2：团队人员',
  `m_updatetime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `m_ip` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_member
-- ----------------------------
INSERT INTO `f_member` VALUES ('1', '超级管理员', 'admin@linshen.com', 'e810fec014e6682880064efafc589659', '1', '0', '2017-07-31 14:43:17', '127.0.0.1');
INSERT INTO `f_member` VALUES ('2', '测试账号', '147258@qq.com', '51bd34d83ec497830dba11e51528adee', '1', '1', '2017-08-28 09:08:20', '127.0.0.1');

-- ----------------------------
-- Table structure for f_member_rel
-- ----------------------------
DROP TABLE IF EXISTS `f_member_rel`;
CREATE TABLE `f_member_rel` (
  `rel_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '团队任务id',
  `m_id` int(11) NOT NULL,
  `t_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`rel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_member_rel
-- ----------------------------
INSERT INTO `f_member_rel` VALUES ('1', '1', '1', '1', '2017-07-06 11:11:21');
INSERT INTO `f_member_rel` VALUES ('2', '2', '2', '2', '2017-07-24 09:07:54');
INSERT INTO `f_member_rel` VALUES ('3', '3', '1', '3', '2017-07-25 15:59:24');
INSERT INTO `f_member_rel` VALUES ('4', '4', '2', '3', '2017-07-25 18:38:07');
INSERT INTO `f_member_rel` VALUES ('5', '5', '2', '3', '2017-07-26 14:31:41');
INSERT INTO `f_member_rel` VALUES ('6', '6', '2', '3', '2017-07-26 14:36:19');
INSERT INTO `f_member_rel` VALUES ('7', '7', '2', '3', '2017-07-26 14:36:58');
INSERT INTO `f_member_rel` VALUES ('8', '8', '2', '3', '2017-07-26 14:37:22');

-- ----------------------------
-- Table structure for f_message
-- ----------------------------
DROP TABLE IF EXISTS `f_message`;
CREATE TABLE `f_message` (
  `t_id` int(11) NOT NULL COMMENT '团队id',
  `t_msg` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '消息内容',
  `t_remark` text COLLATE utf8_unicode_ci COMMENT '扩展字段'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='消息表';

-- ----------------------------
-- Records of f_message
-- ----------------------------
INSERT INTO `f_message` VALUES ('1', '您在2017-07-26 15:11:57执行的抢购任务在2017-07-26 15:14:04执行完毕', '\"s\"');

-- ----------------------------
-- Table structure for f_order
-- ----------------------------
DROP TABLE IF EXISTS `f_order`;
CREATE TABLE `f_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `team_id` int(11) NOT NULL,
  `order_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `order_num` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `order_url` text COLLATE utf8_unicode_ci NOT NULL,
  `goods_countprice` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postage` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `task_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `order_type` char(2) COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1:待付款 2：已付款 未回执 3：已付款已回执',
  `thirdorderid` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pay_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'alipay      阿里支付\r\n            bfb	百度钱包\r\n            cp_b2b	银联企业网银支付，即 B2B 银联 PC 网页支付\r\n            upacp	银联支付，即银联 APP 支付（2015 年 1 月 1 日后的银联新商户使用。若有疑问，请与 Ping++ 或者相关的收单行联系）\r\n            upacp_wap	银联手机网页支付（2015 年 1 月 1 日后的银联新商户使用。若有疑问，请与 Ping++ 或者相关的收单行联系）\r\n            upacp_pc	银联网关支付，即银联 PC 网页支付\r\n            wx	微信 APP 支付\r\n            wx_pub	微信公众号支付\r\n            wx_pub_qr	微信公众号扫码支付\r\n            wx_wap	微信 WAP 支付（此渠道仅针对特定客户开放）\r\n            wx_lite	微信小程序支付\r\n            yeepay_wap	易宝手机网页支付\r\n            jdpay_wap	京东手机网页支付\r\n            fqlpay_wap	分期乐支付\r\n            qgbc_wap	量化派支付\r\n            cmb_wallet	招行一网通\r\n            applepay_upacp	Apple Pay\r\n            mmdpay_wap	么么贷\r\n            qpay	QQ 钱包支付',
  `order_price` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_id` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '，分割只要点击支付链接都记录下来',
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_order
-- ----------------------------

-- ----------------------------
-- Table structure for f_package_price
-- ----------------------------
DROP TABLE IF EXISTS `f_package_price`;
CREATE TABLE `f_package_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ser_type` int(11) DEFAULT NULL,
  `package_remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` char(2) COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1:正常2：关闭',
  `ser_name` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_package_price
-- ----------------------------
INSERT INTO `f_package_price` VALUES ('1', '1', '{\"serverid\":\"meituan\",\"describeTypes\":\"auth\",\"hostcpu\":\"1\",\"hostsdk\":\"70a5a133-25f3-4237-a7a2-e2c1265203fe\",\"sdk\":\"1G\",\"sdktype\":\"C1_M1\",\"zone\":\"auth\"}', '1', '美团云');
INSERT INTO `f_package_price` VALUES ('2', '1', '{\"serverid\":\"meituan\",\"describeTypes\":\"auth\",\"hostcpu\":\"1\",\"hostsdk\":\"8f9fe77a-9457-4fad-9621-2ffd97c7e110\",\"sdk\":\"2G\",\"sdktype\":\"C1_M2\",\"zone\":\"auth\"}', '1', '美团云');
INSERT INTO `f_package_price` VALUES ('3', '2', '{\"serverid\":\"tencat\",\"describeTypes\":\"ap-guangzhou-2:S2\",\"hostcpu\":\"1\",\"hostsdk\":\"1:1\",\"sdk\":\"1G\",\"sdktype\":\"S2.SMALL1\",\"zone\":\"ap-guangzhou-2\"}', '1', '腾讯云');
INSERT INTO `f_package_price` VALUES ('4', '2', '{\"serverid\":\"tencat\",\"describeTypes\":\"ap-guangzhou-2:S2\",\"hostcpu\":\"1\",\"hostsdk\":\"1:2\",\"sdk\":\"2G\",\"sdktype\":\"S2.SMALL2\",\"zone\":\"ap-guangzhou-2\"}', '1', '腾讯云');
INSERT INTO `f_package_price` VALUES ('5', '4', '{\"serverid\":\"ali\",\"describeTypes\":\"cn-qingdao:ecs.XN4\",\"hostcpu\":\"1\",\"hostsdk\":\"ecs.xn4.small\",\"sdk\":\"1G\",\"sdktype\":\"ecs.xn4\",\"zone\":\"cn-qingdao\"}', '1', '阿里云');
INSERT INTO `f_package_price` VALUES ('6', '4', '{\"serverid\":\"ali\",\"describeTypes\":\"cn-qingdao:ecs.N4\",\"hostcpu\":\"1\",\"hostsdk\":\"ecs.n4.small\",\"sdk\":\"2G\",\"sdktype\":\"ecs.n4\",\"zone\":\"cn-qingdao\"}', '1', '阿里云');
INSERT INTO `f_package_price` VALUES ('7', '4', '{\"serverid\":\"ali\",\"describeTypes\":\"cn-qingdao:ecs.N4\",\"hostcpu\":\"2\",\"hostsdk\":\"ecs.n4.large\",\"sdk\":\"4G\",\"sdktype\":\"ecs.n4\",\"zone\":\"cn-qingdao\"}', '1', '阿里云');
INSERT INTO `f_package_price` VALUES ('8', '4', '{\"serverid\":\"ali\",\"describeTypes\":\"cn-qingdao:ecs.sn1ne\",\"hostcpu\":\"2\",\"hostsdk\":\"ecs.sn1ne.large\",\"sdk\":\"4G\",\"sdktype\":\"ecs.sn1ne\",\"zone\":\"cn-qingdao\"}', '1', '阿里云');
INSERT INTO `f_package_price` VALUES ('9', '4', '{\"serverid\":\"ali\",\"describeTypes\":\"cn-beijing:ecs.XN4\",\"hostcpu\":\"1\",\"hostsdk\":\"ecs.xn4.small\",\"sdk\":\"1G\",\"sdktype\":\"ecs.xn4\",\"zone\":\"cn-beijing\"}', '1', '阿里云');
INSERT INTO `f_package_price` VALUES ('10', '2', '{\"serverid\":\"tencat\",\"describeTypes\":\"ap-guangzhou-3:S2\",\"hostcpu\":\"1\",\"hostsdk\":\"1:1\",\"sdk\":\"1G\",\"sdktype\":\"S2.SMALL1\",\"zone\":\"ap-guangzhou-3\"}', '1', '腾讯云');

-- ----------------------------
-- Table structure for f_service
-- ----------------------------
DROP TABLE IF EXISTS `f_service`;
CREATE TABLE `f_service` (
  `ser_id` int(11) NOT NULL AUTO_INCREMENT,
  `ser_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ser_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ser_key` varchar(42) COLLATE utf8_unicode_ci NOT NULL,
  `ser_secret` varchar(42) COLLATE utf8_unicode_ci NOT NULL,
  `ser_type` char(2) COLLATE utf8_unicode_ci NOT NULL COMMENT '1:美团云，2：腾讯云 ,3：ucloud,4:阿里云',
  `t_id` int(11) DEFAULT NULL COMMENT '团队id',
  `is_member` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2' COMMENT '1：超级管理员平台账号 2：团队平台账号',
  PRIMARY KEY (`ser_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_service
-- ----------------------------
INSERT INTO `f_service` VALUES ('4', 'https://mosapi.meituan.com/mcs/v1', '美团云', 'ae333b8e61f6422aa3719510c7fe9c96', '1f7d8b15b26242238ed0c7d65b11eed4', '1', '1', '1');
INSERT INTO `f_service` VALUES ('6', '1251989277', '腾讯云', 'AKID8Ra9AcaeGTYwdgSI2Lqk8DBM605VEfkE', 'jqpoe0aJMKLB6BBUj58CT1Qd96YA4V9x', '2', '1', '1');
INSERT INTO `f_service` VALUES ('7', 'https://mosapi.meituan.com/mcs/v1', '美团云', 'ae333b8e61f6422aa3719510c7fe9c96', '1f7d8b15b26242238ed0c7d65b11eed4', '1', '2', '2');
INSERT INTO `f_service` VALUES ('8', '2017-08-21 10:06:56', '阿里云', 'LTAIeKIaelcx9wLk', '7yawnsmiaimmGMSaDYFhsiVNSqFcWH', '4', '1', '1');
INSERT INTO `f_service` VALUES ('9', '1251989277', '腾讯云', 'AKID8Ra9AcaeGTYwdgSI2Lqk8DBM605VEfkE', 'jqpoe0aJMKLB6BBUj58CT1Qd96YA4V9x', '2', '2', '2');
INSERT INTO `f_service` VALUES ('10', '2017-08-21 10:06:56', '阿里云', 'LTAIeKIaelcx9wLk', '7yawnsmiaimmGMSaDYFhsiVNSqFcWH', '4', '2', '2');

-- ----------------------------
-- Table structure for f_site
-- ----------------------------
DROP TABLE IF EXISTS `f_site`;
CREATE TABLE `f_site` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `site_name` varchar(26) COLLATE utf8_unicode_ci NOT NULL,
  `site_url` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '站点地址',
  `site_status` char(2) COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1:活动 2：关闭',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_site
-- ----------------------------
INSERT INTO `f_site` VALUES ('1', '2017-07-11 09:04:21', '阿迪达斯', 'http://www.adidas.com.cn', '1');
INSERT INTO `f_site` VALUES ('2', '2017-07-04 15:46:31', 'Apple', 'https://www.apple.com/cn/', '2');
INSERT INTO `f_site` VALUES ('3', '2017-07-04 12:07:31', 'nike', 'http://www.nike.com', '1');

-- ----------------------------
-- Table structure for f_task
-- ----------------------------
DROP TABLE IF EXISTS `f_task`;
CREATE TABLE `f_task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `server_num` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_id` int(11) NOT NULL,
  `task_status` char(2) COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1：开启 2：关闭 3:第三方网站下架',
  `task_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `goods_attr` text COLLATE utf8_unicode_ci COMMENT '0表示没有属性',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_task
-- ----------------------------
INSERT INTO `f_task` VALUES ('1', '', '3', '2', 'https://store.nike.com/cn/zh_cn/pd/free-rn-flyknit-2017-%E5%A5%B3%E5%AD%90%E8%B7%91%E6%AD%A5%E9%9E%8B/pid-11591063/pgid-11489229', '');

-- ----------------------------
-- Table structure for f_task_rel
-- ----------------------------
DROP TABLE IF EXISTS `f_task_rel`;
CREATE TABLE `f_task_rel` (
  `task_relid` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL COMMENT '任务id',
  `t_id` int(11) NOT NULL COMMENT '团队id',
  `status` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1:排队等待脚本调用2：抢购结束 3：抢购删除 4：脚本抢购中',
  `task_type` char(2) COLLATE utf8_unicode_ci NOT NULL COMMENT '1:注册 2：抢购',
  `site_id` int(11) NOT NULL COMMENT '平台类别',
  `createtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rules` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '扩展字段，存的团队任务信息',
  PRIMARY KEY (`task_relid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_task_rel
-- ----------------------------

-- ----------------------------
-- Table structure for f_team
-- ----------------------------
DROP TABLE IF EXISTS `f_team`;
CREATE TABLE `f_team` (
  `t_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(28) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '团队名称',
  `defaultpwd` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '团队成员重置之后默认密码',
  `adressmaxdate` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adressmindate` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresskm` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中心点距离',
  `defaultAccountPwd` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_team
-- ----------------------------
INSERT INTO `f_team` VALUES ('1', 'pintai', '2q2W456', '1960-05-25', '2000-01-25', '0.8', 'a123456A');
INSERT INTO `f_team` VALUES ('2', 'ceshi', 'abcABC123', '1961-07-25', '1990-07-25', '1', 'aabbAAbb123');

-- ----------------------------
-- Table structure for f_team_host
-- ----------------------------
DROP TABLE IF EXISTS `f_team_host`;
CREATE TABLE `f_team_host` (
  `host_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_id` int(11) NOT NULL,
  `host_name` varchar(18) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `host_unionid` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `host_config` text COLLATE utf8_unicode_ci,
  `host_createtime` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `host_price` float(6,2) DEFAULT NULL,
  `img_name` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `host_type` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`host_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_team_host
-- ----------------------------
INSERT INTO `f_team_host` VALUES ('16', '2', '', 'i-m5egugzlxg2fke6kna5w', '{\"Img\":{\"img_url\":\"m-m5e34o8c50ub0sbrwcp5\",\"img_name\":\"测试\"},\"taocan\":{\"serverid\":\"ali\",\"describeTypes\":\"cn-qingdao:ecs.XN4\",\"hostcpu\":\"1\",\"hostsdk\":\"ecs.xn4.small\",\"sdk\":\"1G\",\"sdktype\":\"ecs.xn4\",\"zone\":\"cn-qingdao\"},\"taocanid\":\"5\",\"instanceType\":\"ecs.xn4.small\",\"secGroupId\":\"sg-m5eg8nz4nmr02qe4w69r\",\"availabilityZoneId\":\"cn-qingdao\",\"volume\":\"\"}', '1503892112', null, '测试', 'ali');
INSERT INTO `f_team_host` VALUES ('15', '2', '', 'i-m5eikwwxk57ibl1okve1', '{\"Img\":{\"img_url\":\"m-m5e34o8c50ub0sbrwcp5\",\"img_name\":\"测试\"},\"taocan\":{\"serverid\":\"ali\",\"describeTypes\":\"cn-qingdao:ecs.XN4\",\"hostcpu\":\"1\",\"hostsdk\":\"ecs.xn4.small\",\"sdk\":\"1G\",\"sdktype\":\"ecs.xn4\",\"zone\":\"cn-qingdao\"},\"taocanid\":\"5\",\"instanceType\":\"ecs.xn4.small\",\"secGroupId\":\"sg-m5eg8nz4nmr02qe4w69r\",\"availabilityZoneId\":\"cn-qingdao\",\"volume\":\"\"}', '1503892108', null, '测试', 'ali');

-- ----------------------------
-- Table structure for f_team_order
-- ----------------------------
DROP TABLE IF EXISTS `f_team_order`;
CREATE TABLE `f_team_order` (
  `host_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL COMMENT '团队id',
  `pay_price` float(6,2) DEFAULT NULL,
  `order_price` float(6,2) DEFAULT NULL,
  `order_type` char(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1:新开 2：续费',
  `there_order` varbinary(64) DEFAULT NULL,
  `pay_type` char(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'alipay等',
  `ser_id` int(11) DEFAULT NULL,
  `order_remk` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '订单创建时间',
  `starttime` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '主机真正创建时间',
  `endtiem` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '结束时间',
  `order_status` char(2) COLLATE utf8_unicode_ci DEFAULT '1' COMMENT '1：代付款 2：已付款已开通 3：已付款开通失败， 5：已过期',
  PRIMARY KEY (`host_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_team_order
-- ----------------------------

-- ----------------------------
-- Table structure for f_user
-- ----------------------------
DROP TABLE IF EXISTS `f_user`;
CREATE TABLE `f_user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_id` int(11) NOT NULL COMMENT '团队id',
  `u_eamil` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `u_pwd` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `mobile` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机号',
  `birthday` date NOT NULL COMMENT '生日',
  `firstname` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT '中文名收货地址用户名',
  `u_name` varchar(26) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户别名',
  `u_city` varchar(26) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货地址省份',
  `u_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货地址详细信息',
  `u_addresscode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货地址邮政编码',
  `u_field` text COLLATE utf8_unicode_ci NOT NULL COMMENT '扩展字段',
  `u_site_no` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户注册失败平台,逗号分隔平台',
  `u_create_type` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1:excel注册用户 2：随机生成',
  `u_site_ok` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户注册成功网站（1,2,3）逗号分隔注册平台',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of f_user
-- ----------------------------
INSERT INTO `f_user` VALUES ('1', '2', 'xiaomayi165@163.com', 'aabbAAbb123', '15341025674', '1988-05-07', '东行强', 'dongxingqiang1988', '湖北省', '湖北省武汉市汉阳区鹦鹉大道46号天下名企汇', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('2', '2', 'xiaomayi83@163.com', 'aabbAAbb123', '15341750594', '1971-06-06', '虞震琴', 'yuzhenqin1971', '湖北省', '汉阳大道140号(钟家村公交站附近)闽东国际城', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('3', '2', 'xiaomayi124@163.com', 'aabbAAbb123', '15341046663', '1983-06-19', '屈萱林', 'qulin1983', '湖北省', '湖北省武汉市汉阳区汉阳大道134号汉商大厦', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('4', '2', 'xiaomayi42@163.com', 'aabbAAbb123', '17831286374', '1976-10-20', '长孙淑承', 'changsunshucheng1976', '湖北省', '汉正街728附18号金昌大厦', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('5', '2', 'xiaomayi1@163.com', 'aabbAAbb123', '18692434417', '1988-04-29', '安巧凤', 'anqiaofeng1988', '湖北省', '多福路3号汉正街第一大道红宝石座', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('6', '2', 'xiaomayi43@163.com', 'aabbAAbb123', '18440662892', '1986-05-26', '武婵颖', 'wuying1986', '湖北省', '汉阳大道140闽东商务大厦-3A座', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('7', '2', 'xiaomayi166@163.com', 'aabbAAbb123', '17312347220', '1990-04-12', '龚厚初', 'gonghouchu1990', '湖北省', '多福路3号汉正街第一大道蓝宝石座', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('8', '2', 'xiaomayi2@163.com', 'aabbAAbb123', '18525763945', '1987-11-16', '连丽丽', 'lianlili1987', '湖北省', '汉正街多福路汉正街第一大道银座', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('9', '2', 'xiaomayi84@163.com', 'aabbAAbb123', '18591342481', '1981-09-11', '赫连启蓝', 'helianqilan1981', '湖北省', '武汉硚口区沿河大道236-238号中财大厦', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');
INSERT INTO `f_user` VALUES ('10', '2', 'xiaomayi125@163.com', 'aabbAAbb123', '14510063374', '1990-01-13', '武昭彩', 'wuzhaocai1990', '湖北省', '钟家村人信汉商银座-E座', '420105', '{\"addressremark\":{\"country\":\"中国\",\"province\":\"湖北省\",\"city\":\"武汉市\",\"district\":\"汉阳区\",\"street\":\"武胜路辅路\",\"adcode\":\"420105\",\"formatted_address\":\"湖北省武汉市汉阳区武胜路辅路\",\"sematic_description\":\"汉阳造文化创意产业园-55栋西49米\"}}', null, '1', null, '2017-08-28 11:53:59');

# -*- coding: utf-8 -*-
import re
import urllib
import json
import requests
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

dcap = dict(DesiredCapabilities.PHANTOMJS)
dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.221 Safari/537.36 SE 2.X MetaSr 1.0") #设置user-agent请求头
dcap["phantomjs.page.settings.loadImages"] = False #禁止加载图片
browser= webdriver.PhantomJS(desired_capabilities=dcap)
# browser.set_page_load_timeout(15)

spUrl='https://www.apple.com/cn/shop/buy-iphone/iphone-7/5.5-%E8%8B%B1%E5%AF%B8%E5%B1%8F%E5%B9%95-256gb-%E9%87%91%E8%89%B2#01,13,22'
status=''

def find_id_status(id):
	"""
	判断所找id是否被加载出来
	"""
	val=browser.find_elements_by_id(id)

	if len(val) == 0:
	    return False
	if len(val) == 1:
		return True

def find_name_status(name):
	"""
	判断所找name是否被加载出来
	"""  
	val = browser.find_elements_by_name(name)
	if len(val) == 0:
		return False

	if len(val) == 1:
		return True
	


def click_wait_name(name):
	"""
	name点击事件
	"""
	while True:
		if find_name_status(name) == True:
			browser.find_element_by_name(name).click()
			break

def click_wait_id(id):
	"""
	id点击事件
	"""
	while True:
		if find_id_status(id) == True:
			browser.find_element_by_id(id).click()
			break

def send_keys_wait(id,value):
	"""
	输入事件
	"""
	while True:
		if find_id_status(id) == True:
			browser.find_element_by_id(id).send_keys(value)
			break


# 执行块
# browser = webdriver.Chrome()
browser.get(spUrl)
start = time.time()


# 加入购物车加登陆
time.sleep(2)
click_wait_name('add-to-cart')
time.sleep(2)
click_wait_name('proceed')
time.sleep(2)
click_wait_id('cart-actions-checkout')
time.sleep(2)
click_wait_id('guest-checkout')
time.sleep(2)
# print browser.get_cookies()
print browser.page_source
sessionID = browser.page_source.split('"sessionID","value":"')[1].split('"}')[0]

session=requests.Session()
session.cookies=browser.get_cookies()
end = time.time()
print end-start


# head={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
# 			   'Referer': 'http://store.nike.com/cn/zh_cn/'}
# signinUrl='https://secure2.store.apple.com/cn/shop/sentryx/sign_in?_a=login.guestSign&c=aHR0cHM6Ly93d3cuYXBwbGUuY29tL2NuL3Nob3AvYmFnfDFhb3M0MzYxMjI5OWRhYjE0YWM3OThkYzk4NTMyZjE1MWUzZTZhMTMzMThj&_fid=si&r=SXYD4UDAPXU7P7KXF&s=aHR0cHM6Ly9zZWN1cmUyLnN0b3JlLmFwcGxlLmNvbS9jbi9zaG9wL2NoZWNrb3V0L3N0YXJ0P3BsdG49MUYzOEExRjF8MWFvc2JhNTc2M2I4NzNhYzQ3ZWU5Y2IzYzY2ZGZkYzc4OWVhNjE1NWM0OWU&t=SXYD4UDAPXU7P7KXF&up=true'
# infoResp=session.get(signinUrl,headers=head)
# pltn = json.loads(infoResp.text)['head']['data']['args']['pltn']

# checkoutUrl = 'https://secure2.store.apple.com/cn/shop/checkout'
# checkoutHead = { 'Content-Type':'application/x-www-form-urlencoded',
#                 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
#                 'Upgrade-Insecure-Requests':'1',
#                 'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36'}

# data = {'pltn':pltn,
#         'v':'on',
#         'sessionID':'2a0e73ac62d1c34628092977b858f86feae34c50585d818b440d7b38d238bfc3'}

# data = urllib.urlencode(data)
# checkoutResp = session.post(checkoutUrl,headers=checkoutHead,data=data)
# print checkoutResp.text






# click_wait_id('cart-continue-button')

# #添加收货信息
# send_keys_wait('shipping-user-lastName',u'杨')
# send_keys_wait('shipping-user-firstName',u'胜')
# send_keys_wait('shipping-user-daytimePhoneAreaCode','027')
# send_keys_wait('shipping-user-daytimePhone','17786493932')
# send_keys_wait('shipping-user-state',u'湖北')
# TIME_STOP
# send_keys_wait('shipping-user-city',u'武汉')
# TIME_STOP
# send_keys_wait('shipping-user-district',u'汉阳区')
# send_keys_wait('shipping-user-street',u'汉阳造')
# send_keys_wait('shipping-user-street2',u'59栋5楼')
# send_keys_wait('shipping-user-postalCode','432000')
# send_keys_wait('shipping-user-emailAddress','1774507023@qq.com')

# #结算
# click_wait_id('payment-form-options-Alipay-0')
# click_wait_id('payment-continue-button')
# click_wait_id('invoice-next-step')
# click_wait_id('account-continue-as-guest')
# click_wait_id('terms-accept')
# click_wait_id('terms-continue-button')
# click_wait_id('place-order-button')
# click_wait_id('payNow')

# # 部分信息
# time.sleep(2)
# end=time.time()
# print end-start
# print browser.page_source














# browser.find_element_by_name('add-to-cart').click()
# time.sleep(2)
# browser.find_element_by_name('proceed').click()
# time.sleep(2)
# browser.find_element_by_id('cart-actions-checkout').click()
# time.sleep(10)
# browser.find_element_by_id('guest-checkout').click()
# time.sleep(6)
# browser.find_element_by_id('cart-continue-button').click()
# time.sleep(6)
# #输入地址
# browser.find_element_by_id('shipping-user-lastName').send_keys(u'杨')
# browser.find_element_by_id('shipping-user-firstName').send_keys(u'胜')
# browser.find_element_by_id('shipping-user-daytimePhoneAreaCode').send_keys('027')
# browser.find_element_by_id('shipping-user-daytimePhone').send_keys('17786493932')
# browser.find_element_by_id('shipping-user-state').send_keys(u'湖北')
# time.sleep(4)
# browser.find_element_by_id('shipping-user-city').send_keys(u'武汉')
# time.sleep(6)
# browser.find_element_by_id('shipping-user-district').send_keys(u'汉阳区')
# browser.find_element_by_id('shipping-user-street').send_keys(u'汉阳造')
# browser.find_element_by_id('shipping-user-street2').send_keys(u'59栋5楼')
# browser.find_element_by_id('shipping-user-postalCode').send_keys('432000')
# browser.find_element_by_id('shipping-user-emailAddress').send_keys('1774507023@qq.com')
# browser.find_element_by_id('shipping-continue-button').click()
# time.sleep(10)
# #选择付款方式
# browser.find_element_by_id('payment-form-options-Alipay-0').click()
# browser.find_element_by_id('payment-continue-button').click()
# time.sleep(6)
# #发票
# browser.find_element_by_id('invoice-next-step').click()
# time.sleep(2)
# #已访客身份继续
# browser.find_element_by_id('account-continue-as-guest').click()
# time.sleep(2)
# #同意条款协议
# browser.find_element_by_id('terms-accept').click()
# browser.find_element_by_id('terms-continue-button').click()
# time.sleep(2)
# #下单
# browser.find_element_by_id('place-order-button').click()
# time.sleep(10)
# #现在付款
# browser.find_element_by_id('payNow').click()
# time.sleep(2)
# print browser.page_source
# end=time.time()
# print end-start
# browser.close()



# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import sys
import HTMLParser
import json
import cgi
import threading


def adidas(spurl):
	loginHead={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
			   'Referer': 'http://store.nike.com/cn/zh_cn/'}
	spHtml=requests.get(spurl,headers=loginHead)

	soup=BeautifulSoup(spHtml.text,'html.parser')
	script=soup.select('script')[-4]
	productid=str(script).split(',')[2].split('+')[1]
	#print productid
	infoUrl='http://www.adidas.com.cn/specific/product/ajaxview/?id='+productid
	infoHml=requests.get(infoUrl,headers=loginHead)

	infosoup=BeautifulSoup(infoHml.text,'html.parser')
	#sizetext=infosoup.select('#size_box')[0].text

	option=infosoup.select('#size_box option')
	sizeVal=[]
	sizetext=[]
	for tag in option:
		sizeVal.append(tag['value'])
		sizetext.append(tag.text)
		#print tag['value']
	return {"allSize":(sizetext,sizeVal)}


def nike(spurl):
	loginHead={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
			   'Referer': 'http://store.nike.com/cn/zh_cn/'}
	spHtml=requests.get(spurl,headers=loginHead)
	soup=BeautifulSoup(spHtml.text,'html.parser')
	#print spHtml.text
	sizeList=[]
	sizes=soup.select('option')
	for size in sizes:
		sizeList.append(size['value'])
	sizeList = [x for x in sizeList if sizeList.count(x) == 1]
	#print sizeList
	#sizeList=list(set(sizeList))[1:] #处理得到的所有size，删除数组第一个值为空的元素
	sizetext=[]
	sizeVal=[]
	for size in sizeList:
		sizeVal.append(size.split(':')[0])
		sizetext.append(size.split(':')[1])
		# print sizetext
		# print sizeVal
	return {"allSize":(sizetext,sizeVal)}


if __name__ == '__main__':
	spurl='https://store.nike.com/cn/zh_cn/pd/air-zoom-mariah-flyknit-racer-%E7%94%B7%E5%AD%90%E8%BF%90%E5%8A%A8%E9%9E%8B/pid-11598424/pgid-11805238'
	#print sys.argv
	website=sys.argv[2]
	if website=='1':
		print json.dumps(adidas(sys.argv[1]))  # sys.argv[1] #获取php传参
	if website=='3':
		print json.dumps(nike(spurl))

	##调数据
	# loginHead={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
	# 		   'Referer': 'http://store.nike.com/cn/zh_cn/'}
 # 	r=requests.get('http://sale.com/index.php/Rpc/getsite.html',headers=loginHead)
 # 	print r.text

	

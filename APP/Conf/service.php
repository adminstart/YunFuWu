<?php 
return array (
  'ServerProvider' => 
  array (
    '美团云' => 
    array (
      'is_display' => true,
      'val' => 1,
      'isdefault' => 0,
    ),
    '腾讯云' => 
    array (
      'is_display' => true,
      'val' => 2,
      'isdefault' => 0,
    ),
    'UCloud' => 
    array (
      'is_display' => false,
      'val' => 3,
      'isdefault' => 0,
    ),
    '阿里云' => 
    array (
      'is_display' => true,
      'val' => 4,
      'isdefault' => 0,
    ),
    '金山云' => 
    array (
      'is_display' => false,
      'val' => 5,
      'isdefault' => 0,
    ),
  ),
);
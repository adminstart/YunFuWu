<?php
/**
 * Created by PhpStorm.
 * User: leishaofa
 * Date: 2017/7/4
 * Time: 9:28
 */
class SiteAction extends CommonAction
{
    /**
     * @auth:leishaofa
     * @date:2017-07-4
     * @efect:站点列表
     */
    public function lists(){
        $siteModel = M('site','f_',C('flashSale'));
        $number=10;
        if(IS_AJAX){
               $currPage=is_numeric($_GET['p']);
               if($currPage>0){
                $sql="select site_id as keyid,site_name as name,site_url as url,site_status as status from f_site limit ".($currPage-1).",".$number." ";
                $listdata=$siteModel->query($sql);
                return $this->ajaxReturn(array('code'=>101,'msg'=>"成功",'data'=>$listdata));
               }else{
                return $this->ajaxReturn(array('code'=>4001,'msg'=>"没有数据",'data'=>[]));
               }
               exit;
        }
        $sql="select count(*) as countnum from f_site";
        $resultsql=$siteModel->query($sql);
        $this->assign('page',ceil($resultsql[0]['countnum']/$number));
        $this->display();
    }

    /**
     * @auth:leishaofa
     * @date:2017-07-4
     * @parame 添加修改站点  ajax调用
     * @getkeyid $_get['keyid'] 表自增id添加修改依据
    */
    public function addSite(){
        if(IS_AJAX){
            $id=is_numeric($_GET['keyid'])>0?$_GET['keyid']:'';//表自增id
            if(!empty($_POST)){
                $siteModel = M('site','f_',C('flashSale'));
                if (!$siteModel->autoCheckToken($_POST)){
                    // 令牌验证错误
                    parent::ajaxError('表单已提交过！请关闭重新填写！');
                }
                $dao['site_name']=htmlspecialchars($_POST['urlname']);
                $dao['site_url']=filter_var($_POST['url'], FILTER_VALIDATE_URL);
                $status=$_POST['switch']=='on'?1:2;
                if(!empty($id)){

                    $sql="INSERT INTO f_site(site_id,site_url,site_status) VALUES ($id,'".$dao['site_url']."',$status)ON DUPLICATE KEY UPDATE site_url=VALUES(site_url),site_status=VALUES(site_status)";
                    $resultsql=$siteModel->execute($sql);
                    $msg=$resultsql>1?"修改成功":"修改失败";
                }else{
                    $sql="insert IGNORE into f_site(site_name,site_url) values('".$dao['site_name']."','".$dao['site_url']."')";
                    $resultsql=$siteModel->execute($sql);
                    $msg=$resultsql>1?"添加成功":"添加失败";
                }
                if($resultsql>1){
                    parent::ajaxSuccess($msg);
                }else{
                    parent::ajaxError($msg);
                }
                exit;
            }
            //查询修改信息
            if(!empty($id) && empty($_POST)){
                $siteModel = M('site','f_',C('flashSale'));
                $sql="select * from f_site where site_id=".$id." ";
                $resultdata=$siteModel->query($sql);
                if($resultdata){
                    $this->assign('date',$resultdata[0]);
                }
            }
            $this->display();
        }
    }

    /**
     * @auth:leishaofa
     * @date:2017-07-4
     * @parame：删除站点
    */
    public function deleteSite(){
        if(IS_AJAX){

            $site_id=is_numeric($_GET['keyid'])?$_GET['keyid']:'';
            if(!empty($site_id)){

                $siteModel = M('site','f_',C('flashSale'));
                $sql="delete from f_site WHERE site_id=$site_id LIMIT 1";
                $result=$siteModel->execute($sql);
                if($result){
                    parent::ajaxSuccess("删除成功");
                }else{
                    parent::ajaxError("删除失败");
                }

            }else{
             parent::ajaxError("该数据不存在");
            }
        }
    }



}
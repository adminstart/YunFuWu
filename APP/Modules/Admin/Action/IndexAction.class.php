<?php
/**
 * Created by PhpStorm.
 * User: leishaofa
 * Date: 2017/1/28
 * Time: 9:28
 */
class IndexAction extends CommonAction
{
    /**
     * 后台首页
    */
    public function index()
    {
        $memberinfo=session('member_info');
       /* $auth_rule_access = DX('AuthRuleAccess',C('flashSale'));
        $user = DX('User',C('flashSale'));
        $menus = $auth_rule_access->getUserTopNav($memberinfo['m_id']);
        $this->assign('menus', $menus);
        $this->assign('topNavHover',0);*/
        $this->display();
    }

    /**
     * 商家后台左列菜单
    */
    public function category()
    {

        $categoryid=I('post.id');
        if(empty($categoryid)){
            $memberinfo=session('member_info');
            $user = DX('Member',C('flashSale'));
            $auth_rule_access = DX('AuthRuleAccess',C('flashSale'));
            $menus = $auth_rule_access->getUserLeftNav($memberinfo['m_id'],0);
            $this->ajaxReturn(array_values($menus));
            exit;
        }

        if(isset($categoryid) && is_numeric($categoryid)){
            $memberinfo=session('member_info');
            $auth_rule_access = DX('AuthRuleAccess',C('flashSale'));
            var_dump("bbbb");
            $menus = $auth_rule_access->getUserLeftNav($memberinfo['m_id'],$categoryid);
            print_r($menus);
            $this->ajaxReturn($menus);
        }else{
            $this->ajaxReturn(0);
        }
    }

    /**
     *退出登陆
     */
    public function loginOut()
    {
        session_unset();
        session_destroy();
        cookie(null);
        header("Location:/");
        exit;
    }

    /**
     *首页
     */
    public function center()
    {
        $memberinfo=session('member_info');
        $this->assign('member',$memberinfo);
        $model=M('user', 'f_', C('flashSale'));
        $resultxountuser= $model ->query('select count(1) as usercount from f_user where t_id='.$memberinfo['t_id'].' ');
        $this->assign('usertotle',$resultxountuser[0]);
        $otherusertotle=C('MaxPanicBuyingAccount')-$resultxountuser[0]['usercount'];
        $this->assign('otherusertotle',$otherusertotle);
        $userresult= $model->query('select count(1) as usercount from f_member as a left join f_member_rel as b on a.m_id=b.m_id  where a.m_type=2 and t_id='.$memberinfo['t_id'].' ');
        $this->assign('othermembertotle',$userresult[0]);
        $site=$model->query('select site_id,site_name from f_site where site_status = 1');
        //可使用账号
        $siteuser=$model->query('SELECT d.site_id, sum(case when d.u_site_status = 1 and d.u_site_task_status = 1 then 1 else 0 end) as \'keyong\',sum(case when d.u_site_status = 1 and d.u_site_task_status = 2 then 1 else 0 end) as \'suodin\',sum(case when d.u_site_status = 3 then 1 else 0 end) as \'baofei\',sum(case when d.u_site_status = 2 then 1 else 0 end) as \'register\',sum(case when d.u_site_status = 0 then 1 else 0 end) as \'noregister\' FROM f_user AS a INNER JOIN f_user_rel AS d ON a.u_id = d.u_id WHERE a.t_id = '.$memberinfo['t_id'].' GROUP BY d.site_id');

        $site=array_map(function($val) use ($siteuser){
            $data=array_filter($siteuser,function($v) use ($val){
                return $v['site_id'] == $val['site_id']?true:false;
            });
            if($data){
                $data=array_values($data);
                $data[0]['name']=$val['site_name'];
                $val=$data[0];
            }else{
                $val=['keyong'=>0,'suodin'=>0,'baofei'=>0,'register'=>0,'noregister'=>0,'site_id'=>$val['site_id'],'name'=>'site_name'];
            }
            return $val;
        },$site);
       $this->assign('sitedata',$site);
        //
        $this->display();
    }

    /**
     * 循环
     * @param $arr
     * @return array
     */
    private function round($arr)
    {
        krsort($arr);
        $data = array_reduce($arr,function($a,$b){
            $a[] = json_decode($b,true)['num_str'];
            return $a;
        },'');
        return $data;
    }



}
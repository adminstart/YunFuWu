<?php
/**
 * Created by PhpStorm.
 * User: leishaofa
 * Date: 2017/07/31
 * Time: 16:06
 */

class MyServiceAction extends CommonAction{
    /**
    * @auth:leishaofa
    * @date:20170919
    * @efect:获取地域
    */
    public function zone(){
        $memberinfo=session('member_info');
        if(IS_AJAX && IS_POST) {
            $sername = htmlspecialchars($_POST['keyid']);
            $model=M('','',C('flashSale'));
            $where= "where t_id = '".$memberinfo['t_id']."' ";
            $sql="select ser_id,ser_url,ser_key,ser_secret from f_service ".$where." and ser_type = ".$sername." ";
            $resultService=$model->query($sql);
            if(!$resultService){
                $this->ajaxError("请先添写该服务平台信息");
            }
            $data=$resultService[0];
            switch ($sername){
                case 1:
                    $region = array(
                        array(
                            'code'=>'Beijing',
                            'region'=>'Beijing',
                            'name'=>'华北1'
                        ),array(
                            'code'=>'EastChina1',
                            'region'=>'EastChina1',
                            'name'=>'华东1'
                        ),
                    );
                    break;
                case 2:
                    $data['model']='cvm';
                    $actiondata=array('Version'=>'2017-07-17');
                    $resultdata=$this->tencat($data,"DescribeRegions",$actiondata);
                    if(isset($resultdata['Response']['TotalCount']) && $resultdata['Response']['TotalCount']>0){
                        $region=array_map(function ($val){
                            $cache['code']='';
                            $cache['region']=$val['Region'];
                            $cache['name']=$val['RegionName'];
                            return $cache;
                        },$resultdata['Response']['RegionSet']);
                    }
                    break;
                case 4:
                    $actiondata=array();
                    $data['Region']='cn-qingdao';
                    $resultdata = json_decode(json_encode($this->ali($data, '\Ecs\Request\V20140526\DescribeRegionsRequest', $actiondata)),true);//获取组
                    if(isset($resultdata['Regions'])){
                        $region=array_map(function ($val){
                            $cache['code']='';
                            $cache['region']=$val['RegionId'];
                            $cache['name']=$val['LocalName'];
                            return $cache;
                        },$resultdata['Regions']['Region']);
                    }
                    break;
            }
            if($region){
                $this->ajaxSuccess("成功",$region);
            }else{
                $this->ajaxError("失败",[]);
            }
        }

    }

    /**
     * @auth:leishaofa
     * @date:2017-07-31
     * @efect:服务商平台
     */
    public function platfrom(){
        $memberinfo=session('member_info');
        $siteModel = M('service','f_',C('flashSale'));
        $number=10;
        $where=" where t_id = ".$memberinfo['t_id']." ";
        if(IS_AJAX){
            $currPage=is_numeric($_GET['p'])?$_GET['p']:0;
            if($currPage>0){
                $sql="select ser_id as keyid,ser_name as name,ser_url as url,ser_key as apikey,ser_secret as secrets from f_service ".$where." limit ".($currPage-1).",".$number." ";
                $listdata=$siteModel->query($sql);
                $this->ajaxSuccess('成功',$listdata);
            }else{
                $this->ajaxError('没有数据');
            }
            exit;
        }
        $sql="select count(*) as countnum from f_service ".$where." ";
        $resultsql=$siteModel->query($sql);
        $this->assign('page',ceil($resultsql[0]['countnum']/$number));
        $this->display();

    }

    /**
     * @auth:leishaofa
     * @date:2017-07-31
     * @efect 添加修改服务商  ajax调用
     * @parame $_get['keyid'] 表自增id添加修改依据
     */
    public function platfromadd(){
        if(IS_AJAX){
            $id=is_numeric($_GET['keyid'])>0?$_GET['keyid']:'';//表自增id
            $memberinfo=session('member_info');
            if(!empty($_POST)){

                $siteModel = M('service','f_',C('flashSale'));
                if (!$siteModel->autoCheckToken($_POST)){
                    // 令牌验证错误
                    $this->ajaxError('表单已提交过！请关闭重新填写！');
                }
                if(!empty($_GET['keyid']) && empty($id)){
                    $this->ajaxError("修改失败");
                }

                $dao['ser_url']=trim(htmlspecialchars($_POST['url']));
                $dao['ser_key']=trim(htmlspecialchars($_POST['key']));
                $dao['ser_secret']=trim(htmlspecialchars($_POST['secret']));
                if(!empty($id)){
                    $sql="INSERT INTO f_service(ser_id,ser_url,ser_key,ser_secret) VALUES ($id,'".$dao['ser_url']."','".$dao['ser_key']."','".$dao['ser_secret']."')ON DUPLICATE KEY UPDATE ser_url=VALUES(ser_url),ser_key=VALUES(ser_key),ser_secret=VALUES(ser_secret)";
                    $resultsql=$siteModel->execute($sql);
                    $msg=$resultsql>1?"修改成功":"修改失败";
                }else {
                    $dao['ser_type'] = trim($_POST['types']);
                    $servertype=array_map(function($val){ return $val['val'];},C('ServerProvider'));
                    $dao['ser_name'] =  array_search($dao['ser_type'],$servertype);
                    if (!$dao['ser_name']) {
                        $this->ajaxError("服务商未开通");
                    }
                    $is_member = empty($memberinfo['m_type']) ? 1 : 2;
                    $querysql = "select ser_id from f_service where ser_type = '" . $dao['ser_type'] . "' and t_id = " . $memberinfo['t_id'] . " ";
                    $resultISdis = $siteModel->query($querysql);
                    if (!$resultISdis) {
                        $sql = "insert IGNORE into f_service(ser_url,ser_name,ser_key,ser_secret,ser_type,t_id,is_member) values('" . $dao['ser_url'] . "','" . $dao['ser_name'] . "','" . $dao['ser_key'] . "','" . $dao['ser_secret'] . "','" . $dao['ser_type'] . "','" .$memberinfo['t_id'] . "','" . $is_member . "')";
                        $resultsql = $siteModel->execute($sql);
                        $msg = $resultsql > 0 ? "添加成功" : "添加失败";
                    }else{
                        $resultsql=0;
                        $msg="您已经添加了，请不要重复添加哟！";
                    }
                }
                if($resultsql>0){
                    $this->ajaxSuccess($msg);
                }else{
                    $this->ajaxError($msg);
                }
                exit;
            }
            //查询修改信息
            if(!empty($id) && empty($_POST)){
                $siteModel = M('service','f_',C('flashSale'));
                $sql="select ser_id,ser_url,ser_name,ser_key,ser_secret from f_service where ser_id=".$id." ";
                $resultdata=$siteModel->query($sql);
                if($resultdata){
                    $this->assign('date',$resultdata[0]);
                }
            }
            $this->display();


        }
    }

    /**
     * @auth:leishaofa
     * @date:20417803
     * @efect:主机管理
     */
    public function hoslist(){
        $memberinfo=session('member_info');
        $Model = M('','',C('flashSale'));
        $where=' where a.t_id = '.$memberinfo['t_id'];
        $number=12;
        if(IS_AJAX){
            $currPage=is_numeric($_GET['p'])?$_GET['p']:0;
            if($currPage>0){
                $sql="select a.host_id as keyid,a.host_name as hostname,FROM_UNIXTIME(a.host_createtime, '%Y-%m-%d %H:%i:%S') as createtime,a.host_config as remark,host_type from f_team_host as a ".$where." order by a.host_createtime desc   limit ".($currPage-1)*$number.",".$number." ";
                $listdata=$Model->query($sql);
                $servertype=array_map(function($val){ return $val['val'];},C('ServerProvider'));
                $listdata=array_map(function($val) use ($servertype) {
                    $remark=json_decode($val['remark'],true);
                    unset($val['remark']);
                    $val['img_name']=$remark['Img']['img_name'];
                    $val['instanceType']=$remark['taocan']['sdktype'];
                    $val['zone']=$remark['taocan']['zonename'];
                    $val['servername']=array_search($val['host_type'],$servertype);
                    $val['cpu']=$remark['taocan']['hostcpu'];
                    $val['sdk']=$remark['taocan']['sdk'];
                    $val['groupid']=$remark['secGroupId'];
                    $val['ip']=$remark['ip'];
                    $val['password']=empty($remark['password'])?"未绑ip":$remark['password'];
                    $val['linkExtranet']=empty($remark['linkExtranet'])?0:1;
                    return $val;
                },$listdata);
                $this->ajaxSuccess("成功",$listdata);
            }else{
                $this->ajaxError("没有数据",[]);
            }
            exit;
        }
        $sql="select count(1) as countnum from f_team_host as a ".$where;
        $resultsql=$Model->query($sql);
        $this->assign('page',ceil($resultsql[0]['countnum']/$number));
        $this->display();



    }




    /**
     * @auth:leishaofa
     * @date:20170921
     * @efect:获取平台信息
     */
    protected function getFindSite($serid){
        $issitedata=array_filter(C('ServerProvider'),function ($val) use($serid){
            $a=false;
            if($val['val'] == $serid){
                $a= $val['is_display'] == 1?true:false;
            }
            return $a;
        });
        $name=array_keys($issitedata)[0];
        $issitedata=array_values($issitedata)[0];
        $issitedata['name']=$name;
        return $issitedata;
    }
    /**
     *@auth:leishaofa
     *@date:20417802
     * @efect:新增主机
     */
    public function hostadd(){
        if(IS_AJAX){
            $memberinfo=session('member_info');
            $Model = M('','',C('flashSale'));
            if(IS_GET && isset($_GET['ser_id']) && !IS_POST){
                //获取主机套餐
                $serverid = $_GET['ser_id'];
                $server=$this->getFindSite($serverid);
                if(empty($server) || !$server['is_display']){
                    $this->ajaxError("主机未接入");
                }
                $reionid=htmlspecialchars(trim($_GET['regionsid']));
                $regionname=htmlspecialchars(trim($_GET['regionsname']));

                $imgsql="select img_name as name,img_id as id from f_img WHERE img_type  = " . $serverid." and img_status = 1 and img_regions='".$reionid.":".$regionname."' and  t_id = '".$memberinfo['t_id']."' ";
                $hostsql="select package_remark as dataother,id from f_package_price WHERE ser_type  = " . $serverid." and package_remark like '%\"regions\":\"".$reionid."%' and  package_remark like '%\"regionsname\":\"".$regionname."%'   and status = 1 ";
                $resultlist=$Model->query($hostsql);
                foreach($resultlist as $key=>$val){
                    $val['dataother']=json_decode($val['dataother'],true);
                    $packagelist[$val['dataother']['zone'].':'.$val['dataother']['zonename']][$val['dataother']['describeTypes']][]=$val;
                }
                $resultimg=$Model->query($imgsql);
                if(empty($packagelist) && empty($resultimg)) {
                    $this->ajaxError("该地域下面还没有可用镜像和可创建主机");
                }else{
                    $this->ajaxSuccess("", array('host' => $packagelist, 'hostimg' => $resultimg));
                }
            }elseif(IS_POST && empty($_GET['ser_id'])){
                /* if (!$Model->autoCheckToken($_POST)){
                     $this->ajaxError('表单已提交过！请关闭重新填写！'); // 令牌验证错误
                 }*/
                if(!is_numeric($_POST['host'])){
                    $this->ajaxError("主机配置不正确");
                }
                $data=$_POST;
                unset($data['__hash__']);
                $data['hostnumber']=is_numeric($data['hostnumber'])?$data['hostnumber']:'';
                if(empty($data['hostnumber']) || intval($data['hostnumber'])<1){
                    $this->ajaxError("主机开通数量不正确");
                }
                $server= $server=$this->getFindSite($data['serverid']);
                if(empty($server) || !$server['is_display']){
                    $this->ajaxError("主机平台错误");
                }
                //获取套餐
                $data['host']=is_numeric($data['host'])?$data['host']:'';
                $hostsql="select package_remark as dataother,id from f_package_price WHERE ser_type  = '" . $server['val']."' and id = '".$data['host']."'  and status = 1  limit 1 ";
                $packagelist=$Model->query($hostsql)[0];
                if(!$packagelist || empty($data['host'])){
                    $this->ajaxError("主机信息不正确");
                }
                $packagelist['dataother']=json_decode($packagelist['dataother'],true);
                //获取平台配置信息
                $serversql="select ser_url,ser_key,ser_secret from f_service where ser_type = '".$server['val']."' and t_id = ".$memberinfo['t_id']." limit 1 ";
                $resultserver=$Model->query($serversql)[0];
                if(empty($resultserver)){
                    $this->ajaxError("该服务平台->主机商还没有配置");
                }
                $data['imgid']=is_numeric($data['imgid'])?$data['imgid']:'';
                //获取镜像信息
                $imgsql="select img_url,img_name,img_name from f_img where img_id = '".$data['imgid']."' and img_type = '".$server['val']."' and img_status=1 limit 1 ";
                $resultimg=$Model->query($imgsql);
                if(!$resultimg || empty($data['imgid'])){
                    $this->ajaxError("镜像不存在");
                }
                $resultimg=$resultimg[0];
                $sqlval='';
                $hostconfig=array('Img'=>$resultimg,'taocan'=>$packagelist['dataother'],'taocanid'=>$packagelist['id'], 'availabilityZoneId' =>$packagelist['dataother']['zone']);
                $resultserver['Region']=$packagelist['dataother']['regions'];
                if($data['hostTypeByte']==2){
                    $this->timeTaskHost($resultserver,$hostconfig,$memberinfo['t_id'],$data);
                }elseif ($data['hostTypeByte'] !=1 and $data['hostTypeByte'] !=2){
                    $this->ajaxError("请选择创建方式");
                    exit;
                }
                $mbps =C('MBPS')>0 ? C('MBPS') : 2;
                //创建主机商 站点类型调用不同主机
                switch ($server['val']) {
                    case 1:
                        $actiondata = array('Action' => 'CreateInstance', 'ImageId' => $resultimg['img_url'], 'Duration'=>'1H','InstanceType' => $packagelist['dataother']['sdktype'], 'InstanceName' => '', 'ExtraExtDisksize' => 0,'AvailabilityZoneId'=>$packagelist['dataother']['zoneid']);

                        // $actiondata = array('Action' => 'CreateInstance', 'ImageId' => $resultimg['img_url'], 'Duration'=>'1H','InstanceType' => $packagelist['dataother']['hostsdk'], 'InstanceName' => '', 'ExtraExtDisksize' => 0,'AvailabilityZoneId'=>$AvailabilityZoneId[1]);
                        for ($i = 0; $i < intval($data['hostnumber']); $i++) {
                            //创建主机
                            $actiondata['InstanceName'] = 'qianggou'.$memberinfo['t_id'].getorderid();
                            //创建主机
                            $resultdata = $this->meituanyun($resultserver, $actiondata)['CreateInstanceResponse']['Instance'];
                            //创建浮动ip
                            if (is_array($resultdata) && !empty($resultdata)) {
                                $resultip = $this->meituanyun($resultserver, array('Action'=>'AllocateAddress','Name'=>$actiondata['InstanceName'],'AvailabilityZoneId'=>$packagelist['dataother']['zone']))['AllocateAddressResponse']['Address'];

                                //$bangip = $this->meituanyun($resultserver, array('Action' => 'AssociateAddress', 'AllocationId' => $resultip['publicIp'], 'AssociationType' => 'server', 'InstanceId' =>$resultdata['instanceId'], 'Bandwidth' => $mbps));//绑定ip
                                //加入ip白名单
                                file_get_contents('http://http-web.zhimaruanjian.com/index/index/save_white?neek=23963&appkey=13fc3d9d792df9cd5a99f39811a798e6&spec=yangsheng9527&white='.$resultip['publicIp']);
                                $ipid=$resultip['allocationId'];
                                $ipadress=$resultip['publicIp'];
                                $hostconfig['instanceType'] = $resultdata['instanceType'];
                                $hostconfig['ip']=$ipadress;
                                $hostconfig['ipid']=$ipid;
                                $hostconfig['linkExtranet']=0;//$bangip['AssociateAddressResponse']['return'] == 1?1:0;
                                $hostconfig['secGroupId'] = $resultdata['secGroupId'];
                                $hostconfig['volume'] = $resultdata['volume'];
                                if ($i == 0) {
                                    $sqlval = "('" . $memberinfo['t_id'] . "','" . $resultdata['instanceName'] . "','" . $resultdata['instanceId'] . "','" . json_encode($hostconfig, JSON_UNESCAPED_UNICODE) . "','" . strtotime($resultdata['createdAt']) . "','" . $resultimg['img_name'] . "','" . $data['serverid'] . "')";
                                } else {
                                    $sqlval .= ",('" .$memberinfo['t_id'] . "','" . $resultdata['instanceName'] . "','" . $resultdata['instanceId'] . "','" . json_encode($hostconfig, JSON_UNESCAPED_UNICODE) . "','" . strtotime($resultdata['createdAt']) . "','" . $resultimg['img_name'] . "','" . $data['serverid'] . "')";
                                }
                            }else{
                                continue;
                            }
                        }
                        break;
                    case 2:
                        $actiondata=array('Version'=>'2017-08-01','InstanceChargeType'=>'POSTPAID_BY_HOUR','Placement.Zone'=>$packagelist['dataother']['zone'],'InstanceType'=>$packagelist['dataother']['sdktype'],'ImageId'=>$resultimg['img_url'],'InternetAccessible.InternetChargeType'=>'BANDWIDTH_POSTPAID_BY_HOUR','InternetAccessible.InternetMaxBandwidthOut'=>$mbps,'InternetAccessible.PublicIpAssigned'=>'TRUE','InstanceCount'=>intval($data['hostnumber']),'LoginSettings.Password'=>'1q2w3E4R@ls');
                        $resultserver['model']='cvm';
                        $tencatresult=$this->tencat($resultserver,'RunInstances',$actiondata);
                        if(isset($tencatresult['Response']['InstanceIdSet']) && !empty($tencatresult['Response']['InstanceIdSet'])){
                            $tencatresult=$tencatresult['Response']['InstanceIdSet'];
                            for ($i = 0; $i < intval($tencatresult); $i++) {
                                $msgdata[] = $tencatresult[$i];
                                $hostconfig['ip']='公网ip';
                                $hostconfig['ipid']='';
                                $hostconfig['linkExtranet']='3';
                                $hostconfig['instanceType'] = $actiondata['instanceType'];
                                $hostconfig['password']='1q2w3E4R@ls';
                                $hostconfig['secGroupId'] = '';
                                $hostconfig['availabilityZoneId'] = $packagelist['dataother']['zone'];;
                                $hostconfig['volume'] = '';
                                if ($i == 0) {
                                    $sqlval = "('" .$memberinfo['t_id'] . "','" .  $tencatresult[$i] . "','" .  $tencatresult[$i]. "','" . json_encode($hostconfig, JSON_UNESCAPED_UNICODE) . "','" . time() . "','" . $resultimg['img_name'] . "','" . $data['serverid'] . "')";
                                } else {
                                    $sqlval .= ",('" . $memberinfo['t_id']. "','" .  $tencatresult[$i] . "','" .  $tencatresult[$i] . "','" . json_encode($hostconfig, JSON_UNESCAPED_UNICODE) . "','" . time() . "','" . $resultimg['img_name'] . "','" . $data['serverid'] . "')";
                                }
                            }
                        }
                        break;
                    case 4:
                        //安全组
                        $actiondata=array('setRegionId'=>$packagelist['dataother']['regions']);
                        $resultGetGroupAli = json_decode(json_encode($this->ali($resultserver, '\Ecs\Request\V20140526\DescribeSecurityGroupsRequest', $actiondata)),true);//获取组
                        if(empty($resultGetGroupAli['TotalCount'])){
                            //没有组创建组,并设置入网规则
                            $actiondata=array('setRegionId'=>$packagelist['dataother']['regions'],'setSecurityGroupName'=>$packagelist['dataother']['zone']);
                            $resultali = json_decode(json_encode($this->ali($resultserver, '\Ecs\Request\V20140526\CreateSecurityGroupRequest', $actiondata)),true);
                            $groupid=$resultali['SecurityGroupId'];// [SecurityGroupId] => sg-m5egm25gfdxzfgf6kbpl
                            $setgroupdata=array('setSecurityGroupId'=>$groupid,'setSourceCidrIp'=>'0.0.0.0/0','setRegionId'=>$packagelist['dataother']['regions'],'setIpProtocol'=>'tcp','setPortRange'=>'3389/3389');
                            $this->ali($resultserver, '\Ecs\Request\V20140526\AuthorizeSecurityGroupRequest',$setgroupdata);
                            $setgroupdata['setPortRange']='80/80';
                            $this->ali($resultserver, '\Ecs\Request\V20140526\AuthorizeSecurityGroupRequest',$setgroupdata);
                            $setgroupdata['setPortRange']='80/80';
                            $this->ali($resultserver, '\Ecs\Request\V20140526\AuthorizeSecurityGroupRequest',$setgroupdata);
                            $setgroupdata['setPortRange']='443/443';
                            $this->ali($resultserver, '\Ecs\Request\V20140526\AuthorizeSecurityGroupRequest',$setgroupdata);
                            $setgroupdata['setPortRange']='22/22';
                            $this->ali($resultserver, '\Ecs\Request\V20140526\AuthorizeSecurityGroupRequest',$setgroupdata);
                            $setgroupdata['setIpProtocol']='ICMP';
                            $setgroupdata['setPortRange']='-1/-1';
                            $this->ali($resultserver, '\Ecs\Request\V20140526\AuthorizeSecurityGroupRequest',$setgroupdata);
                        }else{
                            $groupid=$resultGetGroupAli['SecurityGroups']['SecurityGroup'][0]['SecurityGroupId'];
                        }
                        $createdata=array('setRegionId'=>$packagelist['dataother']['regions'],'setZoneId'=>$packagelist['dataother']['zoneid'],'setImageId'=>$resultimg['img_url'],'setInstanceType'=>$packagelist['dataother']['hostsdk'],'setSecurityGroupId'=>$groupid,'setInstanceName'=>'','setInternetChargeType'=>'PayByBandwidth','setInternetMaxBandwidthIn'=>$mbps,'setInternetMaxBandwidthOut'=>2,'setPassword'=>'1q2w@3E4R');
                        for ($i = 0; $i < intval($data['hostnumber']); $i++) {
                            $createdata['setInstanceName'] = 'qianggou'.$memberinfo['t_id'].getorderid();
                            $resultCreateAli = json_decode(json_encode($this->ali($resultserver, '\Ecs\Request\V20140526\CreateInstanceRequest', $createdata)),true);//创建主机
                            if(isset($resultCreateAli['InstanceId']) && !empty($resultCreateAli['InstanceId'])) {
                                //分配公网ip
                                $resultipAli = json_decode(json_encode($this->ali($resultserver, '\Ecs\Request\V20140526\AllocatePublicIpAddressRequest', array('setInstanceId'=>$resultCreateAli['InstanceId']))),true);
                                //启动实例
                                $this->ali($resultserver, '\Ecs\Request\V20140526\StartInstanceRequest', array('setInstanceId'=>$resultCreateAli['InstanceId']));
                                $hostconfig['instanceType'] = $createdata['setInstanceType'];
                                $hostconfig['ip']=$resultipAli['IpAddress'];
                                $hostconfig['ipid']='';
                                $hostconfig['password']='1q2w@3E4R';
                                $hostconfig['linkExtranet']=1;
                                $hostconfig['secGroupId'] = $groupid;
                                $hostconfig['availabilityZoneId'] = $packagelist['dataother']['zone'];
                                $hostconfig['volume'] = '';
                                if ($i == 0) {
                                    $sqlval = "('" . $memberinfo['t_id'] . "','" . $actiondata['setInstanceName'] . "','" .$resultCreateAli['InstanceId'] . "','" . json_encode($hostconfig, JSON_UNESCAPED_UNICODE) . "','" . time() . "','" . $resultimg['img_name'] . "','" . $data['serverid'] . "')";
                                } else {
                                    $sqlval .= ",('" . $memberinfo['t_id']. "','" . $actiondata['InstanceName'] . "','" .$resultCreateAli['InstanceId'] . "','" . json_encode($hostconfig, JSON_UNESCAPED_UNICODE) . "','" . time() . "','" . $resultimg['img_name'] . "','" . $data['serverid'] . "')";
                                }
                            }
                        }
                        break;
                    default :
                        break;
                }
                if(!empty($sqlval)) {
                    $sql = "insert IGNORE into f_team_host(t_id,host_name,host_unionid,host_config,host_createtime,img_name,host_type) values " . $sqlval . " ";
                    $resultsql = $Model->execute($sql);
                    $msg = $resultsql > 0 ? "添加成功" : "添加失败";
                }else{
                    $resultsql=0;$msg="添加失败";
                }
                if ($resultsql > 0) {
                    $this->ajaxSuccess($msg,$msgdata);
                } else {
                    $this->ajaxError($msg);
                }
            }
            $this->display();
        }
    }


    /**
     * @auth:leishaofa
     * @date:2017-09-29
     * @efect:创建创建主机定时任务
    */
    public function timeTaskHost($service,$listdata,$tid,$postdata){
        $data=array('services'=>$service,'hostPackage'=>$listdata,'formdata'=>$postdata,'python'=>'2');
        $postData = array('pythontype' => 'Timer', 'name' =>'createHost.py' , 'time' =>$postdata['tasktime']);
       $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, C('PYTHONURL'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $output = curl_exec($ch);
        curl_close($ch);
        print_r($output);
        /*if (preg_match('/^\xEF\xBB\xBF/', $output)) {
            $output = substr($output, 3);
        }
        $output = json_decode(trim($output), true);
        $array = $output['data'];
        $ret = $output['status'];
        if ($ret == 1) {
            $model = M('', '', C('flashSale'));
            $addsql="INSERT INTO f_task_rel (task_id,t_id,rules,status,task_type,createtime,site_id) VALUES(0,".$tid.",'".json_encode($data,JSON_UNESCAPED_UNICODE)."',1,3,'".$postdata['tasktime']."',0) ";
            $result =  $model->execute($addsql);
            if($result){
                $this->ajaxSuccess("定时任务添加成功");
            }else{
                $this->ajaxError("定时任务添加失败");
            }
        }else{
            $this->ajaxError("pythonerror:".$array);
        }*/
        exit;

    }


    /**
     * @auth:leishaofa
     * @date:2017-08-08
     * @efect:主机批量删除
     */
    public function batchdel(){
        $memberinfo=session('member_info');
        if(IS_AJAX){
            $ids=explode(',',$_POST['ids']);
            $ids=array_filter($ids);
            $page=is_numeric($_POST['p'])?$_POST['p']:0;
            $model=M('','',C('flashSale'));
            $where =" where t_id = ".$memberinfo['t_id']." ";
            if($ids[0] == 'on'){
                $startnum=($page-1)*12;
                $query="select COUNT(1) as pcount  from (select host_id from f_team_host ".$where." limit $startnum,12) as subt";
                //$query="select SUM(host_id) as pcount from f_team_host ".$where." limit $startnum,".C('PAGENUMBER')." ";
                $count=$model->query($query);
                if(($count[0]['pcount']+1) != count($ids)){
                    $this->ajaxError("提交数据有错误，请刷新后在尝试");
                }
                unset($ids[0]);
            }
            if($ids !== array_filter($ids,'is_numeric')){
                $this->ajaxError("选中的数据有非数字，请核对和再试");
            }
            $updateid= implode(',',$ids);
            $where.=" and host_id in (".$updateid.")";
            $sql="select * from f_team_host ".$where." ";
            $result=$model->query($sql);
            $deleteid=[];
            foreach ($result as $k=>$v){
                $serversql = "select ser_url,ser_key,ser_secret from f_service where ser_type = '" . $v['host_type'] . "' and t_id = " . $memberinfo['t_id'] . " limit 1 ";
                $server = $model->query($serversql)[0];
                if($v['host_id']==$ids[array_search($v['host_id'],$ids)]) {
                    $remark=json_decode($v['host_config'],true);
                    $server['Region']=$remark['taocan']['regions'];
                    if ( $v['host_type']== 1) {
                        $this->meituanyun($server,array('Action'=>'DisassociateAddress','AllocationId'=>$remark['ipid']));//解绑
                        $resultip=$this->meituanyun($server,array('Action'=>'ReleaseAddress','AllocationId'=>$remark['ipid']));//删除ip
                        if($resultip['ReleaseAddressResponse']['return'] == 1 ){
                            //删除主机
                            $resultdata=$this->meituanyun($server, array('Action' => 'TerminateInstance','InstanceId'=>$v['host_unionid']));
                        }
                        $resultdata['ipstatus']=$resultip['ReleaseAddressResponse']['return'];
                        array_push($deleteid,$v['host_id']);
                    }elseif ($v['host_type'] == 2) {
                        $tencatserver['model']='cvm';
                        $actiondata=array('Version'=>'2017-08-01','InstanceIds.1'=>$v['host_unionid']);
                        $tencatresult=$this->tencat($server,'TerminateInstances',$actiondata);
                       // if(!isset($tencatresult['Error'])){
                            array_push($deleteid,$v['host_id']);
                        //}
                    }elseif ($v['host_type'] == 4) {
                        $this->ali($server, '\Ecs\Request\V20140526\StopInstanceRequest', array('setInstanceId'=>$v['host_unionid'],'setForceStop'=>'true','setConfirmStop'=>'true'));
                        sleep(2);
                        $resultdata = json_decode(json_encode($this->ali($server, '\Ecs\Request\V20140526\DeleteInstanceRequest', array('setInstanceId'=>$v['host_unionid']))),true);
                        if(is_array($resultdata)){
                            array_push($deleteid,$v['host_id']);
                        }
                    }
                }
            }
            $deletewhere=" where t_id = ".$memberinfo['t_id']."  and host_id in (".implode(',',$deleteid).")";
            $sql="DELETE FROM f_team_host ".$deletewhere." ";

            $delcount=$model->execute($sql);
            parent::ajaxSuccess("成功删除".$delcount."条数据");
        }
    }


    /**
     * @auth:leishaofa
     * @date:2017-09-06
     * @efect:批量绑定所有ip未与主机绑定的ip
     */
    public function batchLink(){
        if(IS_AJAX){
            $memberinfo=session('member_info');
            $model=M('','',C('flashSale'));
            $mbps =C('MBPS')>0 ? C('MBPS') : 2;
            $updatevalue='';
            $countsucess=0;
            $counterror=0;
            foreach (C('ServerProvider') as $ck=>$cv){
                if($cv['is_display']) {
                    $serversql = "select ser_url,ser_key,ser_secret from f_service where ser_type = '" . $cv['val'] . "' and t_id = " .  $memberinfo['t_id']. " limit 1 ";
                    $resultserver = $model->query($serversql);
                    $resultserver = $resultserver[0];
                    $sql = "select host_id,host_type,host_unionid,host_config from f_team_host where host_type = '" . $cv['val'] . "' and  t_id='" .  $memberinfo['t_id'] . "' and  host_config like '%\"linkExtranet\":\"0\"%'";
                    $list = $model->query($sql);
                    if (!empty($list)) {
                        switch ($cv['val']) {
                            case 1:
                                foreach ($list as $key => $val) {
                                    $remark = json_decode($val['host_config'], true);
                                    $resultserver['Region']=$remark['taocan']['regions'];
                                    $actiondata = array('Action' => 'AssociateAddress', 'AllocationId' => $remark['ip'], 'AssociationType' => 'server', 'InstanceId' => $val['host_unionid'], 'Bandwidth' => $mbps);
                                    $resultip = $this->meituanyun($resultserver, $actiondata);
                                    if ($resultip['AssociateAddressResponse']['return'] == 1) {
                                        $countsucess++;
                                        $remark['linkExtranet'] = 1;
                                        $resultpassword=$this->meituanyun($resultserver, array('Action' =>'GetPasswordData','InstanceId'=>$val['host_unionid']));
                                        $remark['password']=$resultpassword['GetPasswordDataResponse']['passwordData'];
                                        if (empty($updatevalue)) {
                                            $updatevalue = "('" . $val['host_id'] . "','" . json_encode($remark,JSON_UNESCAPED_UNICODE) . "')";
                                        } else {
                                            $updatevalue .= ",('" . $val['host_id'] . "','" . json_encode($remark,JSON_UNESCAPED_UNICODE) . "')";
                                        }

                                    } else {
                                        $counterror++;
                                    }
                                }
                                break;
                            case 2:
                                foreach ($list as $key => $val) {
                                    $remark = json_decode($val['host_config'], true);
                                    $resultserver['Region']=$remark['availabilityZoneId'];
                                    $resultserver['model']='eip';
                                    $actiondata=array('eipId'=>$remark['ipid'],'unInstanceId'=>$val['host_unionid']);
                                    $tencatresult=$this->tencat($resultserver,'EipBindInstance',$actiondata);
                                    if ($tencatresult['codeDesc'] == 'Success') {
                                        $countsucess++;
                                        $remark['linkExtranet'] = 1;
                                        if (empty($updatevalue)) {
                                            $updatevalue = "('" . $val['host_id'] . "','" . json_encode($remark,JSON_UNESCAPED_UNICODE) . "')";
                                        } else {
                                            $updatevalue .= ",('" . $val['host_id'] . "','" . json_encode($remark,JSON_UNESCAPED_UNICODE) . "')";
                                        }
                                    } else {
                                        $counterror++;
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            if(!empty($updatevalue)) {
                $updatesql = "insert into f_team_host(host_id, host_config) values ".$updatevalue." on duplicate key update host_config=values(host_config);";
                $model->execute($updatesql);
            }
            parent::ajaxSuccess("绑定成功".$countsucess."条数据失败了".$counterror."条数据");
            exit;
        }
    }


    /**
     * @auth:leishaofa
     * @date:2017-08-18
     * @efect:主机单个删除
     */
    public function onedel(){
        if(IS_AJAX){
            $memberinfo=session('member_info');
            $updateid=is_numeric($_GET['keyid'])?$_GET['keyid']:'';
            $model=M('','',C('flashSale'));
            $where =" where t_id = ".$memberinfo['t_id']." ";
            if(empty($updateid)){
                $this->ajaxError("请核对和再试，删除主机信息有误");
            }
            $where.=" and host_id = ".$updateid." ";
            $sql="select * from f_team_host ".$where." ";
            $result=$model->query($sql);
            $serversql="select ser_url,ser_key,ser_secret from f_service where ser_type = '".$result[0]['host_type']['val']."' and t_id = ".$memberinfo['t_id']." limit 1 ";
            $resultserver=$model->query($serversql)[0];
            $remark=json_decode($result[0]['host_config'],true);
            $resultserver['Region']=$remark['taocan']['regions'];
            if ($result[0]['host_type']['val'] == 1) {
                $this->meituanyun($resultserver,array('Action'=>'DisassociateAddress','AllocationId'=>$remark['ipid']));
                $resultip=$this->meituanyun($resultserver,array('Action'=>'ReleaseAddress','AllocationId'=>$remark['ipid']));
                if($resultip['ReleaseAddressResponse']['return'] == 1 ){
                    $resultdata=$this->meituanyun($resultserver,array('Action' => 'TerminateInstance','InstanceId'=>$result[0]['host_unionid']));
                }
                $resultdata=$resultdata['TerminateInstanceResponse'];
            }elseif ($result[0]['host_type']['val'] == 2) {
                $resultserver['model']='cvm';
                $resultdata=$this->tencat($resultserver,'TerminateInstances',array('Version'=>'2017-08-01','InstanceIds.1'=>$result[0]['host_unionid']));
                if(isset($resultdata['Error'])){
                    $this->ajaxError($resultdata['Error']['Message']);
                    exit;
                }
            }elseif ($result[0]['host_type']['val'] == 4) {
                $this->ali($resultserver, '\Ecs\Request\V20140526\StopInstanceRequest', array('setInstanceId'=>$result[0]['host_unionid'],'setForceStop'=>'true','setConfirmStop'=>'true'));
                sleep(2);
                $resultdata = json_decode(json_encode($this->ali($resultserver, '\Ecs\Request\V20140526\DeleteInstanceRequest', array('setInstanceId'=>$result[0]['host_unionid']))),true);
                if(!is_array($resultdata)){
                    $this->ajaxError($resultdata);
                }
            }
            if(isset($resultdata) && !empty($resultdata) && is_array($resultdata)) {
                 $sql="DELETE FROM f_team_host ".$where." ";
                 $delcount=$model->execute($sql);
                 $this->ajaxSuccess("成功删除".$delcount."条数据");
            }else{
                $this->ajaxError("删除失败");
            }
        }
    }




    /**
     * @auth:leishaofa
     * @date:2017-08-01
     * @efect:镜像列表
     */
    public function imgTable(){

        $siteModel = M('service','f_',C('flashSale'));
        $memberinfo=session('member_info');
        $number=10;
        $where= "where a.t_id = '".$memberinfo['t_id']."' ";
        if(IS_AJAX){
            $currPage=is_numeric($_GET['p'])?$_GET['p']:0;
            if($currPage>0){

                $sql="select a.img_id as keyid,a.img_name as name,a.img_url as url,a.img_type as type,a.img_regions as regionstitle from f_img as a ".$where." limit ".($currPage-1)*$number.",".$number." ";
                $listdata=$siteModel->query($sql);

                foreach ($listdata as $key=>$val) {
                    //print_r(explode(':',$val['img_regions']));
                    $listdata[$key]['regionstitle']=explode(':',$val['regionstitle'])[1];
                    foreach (C('ServerProvider') as $k=>$v){
                        if($v['val']==$val['type']){
                            $listdata[$key]['type']=[$k];
                            break;
                        }else{
                            continue;
                        }
                    }
                }
                $this->ajaxSuccess("成功",$listdata);
            }else{
                $this->ajaxError("没有数据",[]);
            }
            exit;
        }
        $sql="select count(1) as countnum from f_img as a ".$where." ";
        $resultsql=$siteModel->query($sql);
        $this->assign('page',ceil($resultsql[0]['countnum']/$number));
        $this->display();




    }

    /**
     * @auth:leishaofa
     * @date:2017-07-31
     * @efect 添加镜像
     * @parame $_get['keyid'] 表自增id添加修改依据
     *///        var url = "{:U('MyService/discribeRegions')}";
    // var url = "{:U('MyService/imgGetlow')}";
    public function imgInsert(){
        if(IS_AJAX){
            $memberinfo=session('member_info');
            $id=is_numeric($_GET['keyid'])>0?$_GET['keyid']:'';//表自增id
            if(empty($id) && $_GET['keyid']){
                $this->ajaxError("镜像不存在");
            }
            $Model = M('','',C('flashSale'));
            if(!empty($_POST)){
                if (!$Model->autoCheckToken($_POST)){parent::ajaxError('表单已提交过！请关闭重新填写！');}
                if($id){
                    $imgstatus=filter_var($_POST['isstatus'],FILTER_VALIDATE_BOOLEAN)?1:2;
                    $sql="INSERT INTO f_img(img_status,img_id) VALUES ('".$imgstatus."',$id)ON DUPLICATE KEY UPDATE img_status=VALUES(img_status)";
                    $resultsql=$Model->execute($sql);
                    $msg=$resultsql>0?"修改成功":"修改失败";
                    if($resultsql>0){
                        parent::ajaxSuccess($msg);
                    }else{
                        parent::ajaxError($msg);
                    }
                }
                if(!$_POST['imgid']){$this->ajaxError("请选择镜像");}
                $sername=htmlspecialchars($_POST['serverid']);
                $issitedata=array_filter(C('ServerProvider'),function ($val) use($sername){
                    $a=false;
                    if($val['val'] == $sername){
                        $a= $val['is_display'] == 1?true:false;
                    }
                    return $a;
                });
                $issitedata=array_values($issitedata);
                if(!$issitedata[0]['is_display']){
                    $this->ajaxError("平台正在接入中");
                }
                $imgid=$_POST['imgid'];
                $insertvalues='';
                $title=$_POST['title'];
                $imgregions=htmlspecialchars($_POST['imgregions']).":".htmlspecialchars($_POST['imgregionstitle']);
                if(is_array($imgid)){
                    foreach ($imgid as $k=>$v){
                        $name= array_reduce($title,function($a,$val) use ($k){
                            if($val['imgid']==$k){$a=$val['name'];}
                            return $a;
                        },"");
                        $status=filter_var($v,FILTER_VALIDATE_BOOLEAN)?1:2;
                        if (empty($insertvalues)) {
                            $insertvalues="('".$name."','".$memberinfo['t_id']."','".$sername."','".$k."','".$status."','".$imgregions."')";
                        }else{
                            $insertvalues.=",('".$name."','".$memberinfo['t_id']."','".$sername."','".$k."','".$status."','".$imgregions."')";
                        }
                    }
                }

                $sql="insert IGNORE into f_img(img_name,t_id,img_type,img_url,img_status,img_regions) values ".$insertvalues." ";
                $resultsql=$Model->execute($sql);
                $msg=$resultsql>0?"添加成功":"添加失败";
                if($resultsql>0){
                    $this->ajaxSuccess($msg);
                }else{
                    $this->ajaxError($msg);
                }
                exit;
            }
            //查询修改信息
            if(!empty($id)){
                $sql="select * from f_img where t_id = '".$memberinfo['t_id']."' and img_id=".$id." ";
                $resultdata=$Model->query($sql);

                if($resultdata){
                    foreach (C('ServerProvider') as $key=>$val){
                        if($resultdata[0]['img_type']==$val['val']){
                            $resultdata[0]['tyepname']=$key;
                        }
                    }
                    $resultdata[0]['img_regions']=explode(':',$resultdata[0]['img_regions']);
                    $this->assign('date',$resultdata[0]);
                }

            }
            $this->display();
        }
    }

    /**
     *@auth:leishaofa
     *@date:20417802
     * @efect:获取美团镜像
     */
    public function imgGetlow(){
        if(IS_AJAX && IS_POST){
            $memberinfo=session('member_info');
            $where= "where t_id = '".$memberinfo['t_id']."' ";
            $model=M('','',C('flashSale'));
            $site_type=htmlspecialchars($_POST['keyid']);
            $issitedata=array_filter(C('ServerProvider'),function ($val) use($site_type){
                $a=false;
                if($val['val'] == $site_type){
                    $a= $val['is_display'] == 1?true:false;
                }
                return $a;
            });
            $issitedata=array_values($issitedata);
            if(!$issitedata[0]['is_display']){
                $this->ajaxError("平台正在接入中");
            }
            $sql="select ser_id,ser_url,ser_key,ser_secret from f_service ".$where." and ser_type = ".$site_type." ";
            $resultService=$model->query($sql)[0];
            if(!$resultService){
                $this->ajaxError("请先添写该服务平台信息");
            }
            $regions=$_POST['regions'];
            $resultService['Region']=$regions;
            //平台区分
            if($site_type == 1){
                $actiondata=array('Action'=>'DescribeTemplates');
                $resultImgdata=$this->meituanyun($resultService,$actiondata)['DescribeTemplatesResponse']['TemplateSet']['Template'];
                $resultImgdata=array_filter($resultImgdata,function($val){
                    return empty($val['is_public'])?1:0;
                });
                if(!is_array($resultImgdata) || empty($resultImgdata)){
                    $this->ajaxError("该平台你还没有私有镜像");
                }
                $overquerysql="select img_url from f_img ".$where." and img_type = ".$site_type." ";
                $resuctover=$model->query($overquerysql);
                if($resuctover) {//已存在静止选择
                    $resultImgdata = array_map(function ($val) use ($resuctover) {
                        $val['is_over']=array_reduce($resuctover,function($a,$v) use ($val){
                            if($val['templateId'] == $v['img_url']){
                                $a=1;
                            }
                            return $a;
                        },0);
                        return $val;
                    }, $resultImgdata);
                }
                $this->ajaxSuccess("",$resultImgdata);
            }elseif ($site_type == 2){
                $resultService['model']='image';
                //$actiondata=array('Version'=>'2017-07-06');
                $actiondata=array('imageType'=>1);
                $resultdata=$this->tencat($resultService,"DescribeImages",$actiondata);
                if($resultdata['code']==0){
                    if($resultdata['totalCount'] ==0){
                        $this->ajaxError("账号下面还没有私有镜像");
                    }else{
                        $overquerysql="select img_url from f_img ".$where." and img_type = ".$site_type." ";
                        $resuctover=$model->query($overquerysql);

                        $resultdata['imageSet']=array_map(function ($val) use ($resuctover){
                            $data=["status"=>2,"templateName"=>$val['imageName'],"checksum"=>null,"templateId"=>$val['unImgId'],"is_public"=>$val['imageType'],"size"=>$val['size']];
                            $data['is_over']=array_reduce($resuctover,function($a,$v) use ($val){
                                if($val['unImgId'] == $v['img_url']){
                                    $a=1;
                                }
                                return $a;
                            },0);
                            return $data;

                        },$resultdata['imageSet']);
                        $this->ajaxSuccess("",$resultdata['imageSet']);
                    }
                }else{
                    $this->ajaxError("API错误：".$resultdata['message']);
                }
            }elseif ($site_type == 4){
                //var_dump($_POST);
                $actiondata=array('setRegionId'=>$regions,'setImageOwnerAlias'=>'self','setPageSize'=>50);
                $resultali = json_decode(json_encode($this->ali($resultService, '\Ecs\Request\V20140526\DescribeImagesRequest', $actiondata)),true);
                if(!is_array($resultali)) {
                    $this->ajaxError("阿里云API错误");
                }
                if($resultali['TotalCount'] ==0){
                    $this->ajaxError("账号该地域私有镜像");
                }else{
                    $overquerysql="select img_url from f_img ".$where." and img_type = ".$site_type." ";
                    $resuctover=$model->query($overquerysql);

                    $resultdata['imageSet']=array_map(function ($val) use ($resuctover){
                        $data=["status"=>2,"templateName"=>$val['ImageName'],"checksum"=>null,"templateId"=>$val['ImageId'],"is_public"=>$val['OSType'],"size"=>$val['Size']];
                        $data['is_over']=array_reduce($resuctover,function($a,$v) use ($val){
                            if($val['ImageId'] == $v['img_url']){
                                $a=1;
                            }
                            return $a;
                        },0);
                        return $data;

                    },$resultali['Images']['Image']);
                    $this->ajaxSuccess("",$resultdata['imageSet']);
                }
            }
        }
    }

    /**
     * @auth:leishaofa
     * @data:20170823
     * @efect:获取服务商可选择的机房（地域列表）
     */
    /* public function discribeRegions(){
         if(IS_AJAX && IS_POST) {
             $memberinfo=session('member_info');
             $where= "where t_id = '".$memberinfo['t_id']."' ";
             $sername=htmlspecialchars($_POST['keyid']);
             $model=M('','',C('flashSale'));
             $sertype=C('ServerProvider')[$sername];
             if(!$sertype['is_display']){
                 $this->ajaxError("平台正在接入中");
             }
             if(empty($sertype)){
                 $this->ajaxError("数据错误");
             }
             $sql="select ser_id,ser_url,ser_key,ser_secret from f_service ".$where." and ser_type = ".$sertype['val']." ";
             $resultService=$model->query($sql);
             if(!$resultService){
                 $this->ajaxError("请先添写该服务平台信息");
             }
             $data=$resultService[0];
             if ($sername == "meituan") {
                 $actiondata = array('Action' => 'DescribeAvailabilityZones');
                 $resultImgdata = $this->meituanyunComputerRoom($data, $actiondata);
                 $this->ajaxSuccess('',$resultImgdata);
                 exit;
             }
             //平台区分
             //$data=$resultService[0];
             $result=array_keys(getdiscribedata(C('ServerProvider')[$sername]['val']));
             if(empty($result)){
                 $this->ajaxSuccess('未知服务商');
             }
             $pinyin=new Overtrue\Pinyin\Pinyin();
             $resultdata=array();
             //组装平台regions
             foreach ($result as $key=>$val) {
                 $cachedata = $pinyin->convert($val);
                 if(in_array('qu',$cachedata)){
                     unset($cachedata[count($cachedata)-1]);
                 }
                 if ($sername == "meituan") {
                     $actiondata = array('Action' => 'DescribeAvailabilityZones');
                     $data['Region']='EastChina1';
                     $resultImgdata = $this->meituanyun($data, $actiondata);
                     $cachedata=implode($cachedata);
                     $resultdata[$cachedata]=$val;
                 } elseif ($sername == "tencat") {
                     $valarray=preg_split('/(?<!^)(?!$)/u', $val );//中文字符拆分数组
                     $cachedata=array_filter($cachedata,function($v){return !is_numeric($v);});
                     $valarray=array_slice($valarray, 0, count($cachedata));
                     $resultdata['ap-'.implode($cachedata)]=implode($valarray);
                 } elseif ($sername == "ali") {
                     $cachedata=implode($cachedata);
                     $resultdata['cn-' . $cachedata] = $val;
                 } else {
                     $this->ajaxError("服务商接入中");
                     exit;
                 }
             }
             unset($result);
             $this->ajaxSuccess('',$resultdata);
         }
     }*/

    /**
     * @auth:leishaofa
     * @date:2017-08-01
     * @efect 删除镜像  ajax调用
     * @parame $_get['keyid'] 表自增id添加修改依据
     */
    public function delImg(){
        if(IS_AJAX){
            $site_id=is_numeric($_POST['keyid'])?$_POST['keyid']:'';
            if(!empty($site_id)){
                $siteModel = M('site','f_',C('flashSale'));
                $memberinfo=session('member_info');
                $sql="delete from f_img WHERE img_id='".$site_id."' and t_id = '".$memberinfo['t_id']."'";
                $result=$siteModel->execute($sql);
                if($result){
                    parent::ajaxSuccess("删除成功",$result);
                }else{
                    parent::ajaxError("删除失败",$result);
                }
            }else{
                parent::ajaxError("该数据不存在");
            }
        }

    }


}
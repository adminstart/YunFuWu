<?php
/**
 * Created by PhpStorm.
 * User: leishaofa
 * Date: 2017/8/1
 * Time: 11:48
 * efect:团队订单
 */
class AllOrderAction extends CommonAction{

    /**
     * @auth:leishaofa
     * @date:2017-07-13
     * @efect:我的订单列表
     */
    public function allOrder(){
        $number=8;
        $ordermodel=M('order','f_',C('flashSale'));
        $memberinfo = session('member_info');
        $page=is_numeric($_GET['p'])?$_GET['p']:'';
        $where='';
        if(IS_AJAX && $page){
            $startnumber=($page-1)*$number;
            $field="a.order_id,a.order_name,a.order_num,a.order_url,a.goods_countprice,a.postage,a.order_type,a.thirdorderid,a.pay_type,a.order_price,a.remark,a.createtime,b.u_eamil,b.u_pwd,b.firstname,b.u_city,b.u_address,b.u_addresscode,b.u_field,c.site_name,d.t_name";
            $datasql="select ".$field." from f_order as a left join f_user as b on a.u_id=b.u_id left join f_team as d on a.team_id=d.t_id   left join f_site as c on a.site_id=c.site_id  ".$where."  order by a.createtime desc limit $startnumber, ".$number."  ";
            $listdata = $ordermodel->query($datasql);
            if (is_array($listdata) && !empty($listdata)) {
                $listdata=array_map(function($val){
                    $val['remark']=json_decode($val['remark'],true);
                    $val['u_field']=json_decode($val['u_field'],true);
                    return $val;
                },$listdata);
                return $this->ajaxReturn(array('code' => 101, 'msg' => "成功", 'data' => $listdata));
            } else {
                return $this->ajaxReturn(array('code' => 4001, 'msg' => "没有数据", 'data' => []));
            }
            exit;
        }
        $resultcountsql="select count(1) as ordersum from f_order as a  ".$where." ";
        $resultcount=$ordermodel->query($resultcountsql);
        $this->assign('page',ceil($resultcount[0]['ordersum']/$number));
        C('TOKEN_ON',false);
        $this->display();
    }


    /**
     * @auth:leishaofa
     * @date:2017-07-24
     * @efect: 获取支付链接
     */
    public function goPay(){
        if(is_ajax) {
            $memberinfo = session('member_info');
            $id = is_numeric($_POST['id']) ? $_POST['id'] : 0;
            if (empty($id)) {
                $this->ajaxError("没有获取到订单号");
            }
            $resultcountsql = "select order_url,m_id,site_id from f_order where order_id= " . $id . " and team_id = " . $memberinfo['t_id'] . " ";
            $resultdata = M('order', 'f_', C('flashSale'))->query($resultcountsql);
            /*  $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, trim($url));
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36');
              curl_setopt ($ch,CURLOPT_REFERER,'https://www.adidas.com.cn/customer/account/login/');
              curl_setopt($ch, CURLOPT_HEADER,1);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
              curl_setopt($ch, CURLOPT_AUTOREFERER, true);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
               $result= curl_exec($ch);*/
            //记录点击支付的记录
            $m_id = explode(',', $resultdata[0]['m_id']);
            if(!in_array($memberinfo['m_id'],$m_id)) {
                array_push($m_id, $memberinfo['m_id']);
                $m_id = implode(",", $m_id);
                $resultcountsql = "update from f_order set m_id=" . $m_id . " where order_id= " . $id . " and team_id = " . $memberinfo['t_id'] . " ";
                M('order', 'f_', C('flashSale'))->execute($resultcountsql);
            }

            $postData=array('pythontype'=>'getPay','order_url'=>$resultdata[0]['order_url'],'site_id'=>$resultdata[0]['site_id']);
           // $url = "";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, C('PYTHONURL'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// post数据
            curl_setopt($ch, CURLOPT_POST, 1);
// post的变量
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $output = curl_exec($ch);
            curl_close($ch);
            if(preg_match('/^\xEF\xBB\xBF/',$output)){
                $output = substr($output,3);
            }
            $output = json_decode(trim($output),true);
            $array=$output['data'];
            $ret=$output['status'];
           // $output=json_decode($output,true);
            //$array=$output['data'];
            //$ret=$output['status'];

           // exec('python ' . dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '\python2\getPay.py "' . $resultdata[0]['order_url'] . '" '.$resultdata[0]['site_id'], $array, $ret);
            if ($ret == 1) {
                $this->ajaxError("脚本未运行");
            }if ($ret == 2) {
                $this->ajaxError($array);
            }  else {
                $this->ajaxSuccess("成功", '<img src="' . $array[0] . '"/>');
            }
        }
    }

    /**
     * @auth:leishaofa
     * @date:2017-07-25
     * @efect: 确认支付完成
    */
    public function confirmGoPay(){
        if(IS_AJAX){
            $memberinfo = session('member_info');
            $id = is_numeric($_POST['id']) ? $_POST['id'] : 0;
            $resultcountsql = "update f_order set order_type=2 where order_id= " . $id . " and team_id = " . $memberinfo['t_id'] . " ";
            $resultup=M('order', 'f_', C('flashSale'))->execute($resultcountsql);
            if ($resultup) {
                $this->ajaxSuccess("操作成功");
            } else {
                $this->ajaxError("操作失败，订单不存在");
            }
        }
    }
    /**
     * @auth:leishaofa
     * @date:2017-07-25
     * @efect: 团队删除订单
     */
    public function delorder(){
        if(IS_AJAX){
            $memberinfo = session('member_info');
            $id = is_numeric($_POST['keyid']) ? $_POST['keyid'] : 0;
            $resultcountsql = "update f_order set order_type=3 where order_id= " . $id . " and team_id = " . $memberinfo['t_id'] . " ";
            $resultup=M('order', 'f_', C('flashSale'))->execute($resultcountsql);
            if ($resultup) {
                $this->ajaxSuccess("删除成功");
            } else {
                $this->ajaxError("操作失败，订单不存在");
            }
        }
    }
    /**
     * @auth:leishaofa
     * @date:2017-07-25
     * @efect: 团队删除订单
     */
    public function getExpress(){
        if(IS_AJAX){
            $memberinfo = session('member_info');
            $id = is_numeric($_POST['keyid']) ? $_POST['keyid'] : 0;
            if (empty($id)) {
                $this->ajaxError("没有获取到订单号");
            }
            $resultcountsql = "select a.thirdorderid,a.site_id,b.u_eamil from f_order as a left join f_user as b on a.u_id=b.u_id where order_id= " . $id . " and team_id = " . $memberinfo['t_id'] . " ";
            $resultdata = M('order', 'f_', C('flashSale'))->query($resultcountsql);
            $postData=array('pythontype'=>'orderInfo','thirdorderid'=>$resultdata[0]['thirdorderid'],'u_eamil'=>$resultdata[0]['u_eamil'],'site_id'=>$resultdata[0]['site_id']);
            // $url = "";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, C('PYTHONURL'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// post数据
            curl_setopt($ch, CURLOPT_POST, 1);
// post的变量
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $output = curl_exec($ch);
            curl_close($ch);
            if(preg_match('/^\xEF\xBB\xBF/',$output)){
                $output = substr($output,3);
            }
            $output = json_decode(trim($output),true);
            $array=$output['data'];
            $ret=$output['status'];
            //$output=json_decode($output,true);
            //$array=$output['data'];
            //$ret=$output['status'];
            //exec('python ' . dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '\python2\orderInfo.py '.$resultdata[0]['thirdorderid'].' '.$resultdata[0]['u_eamil'].' '.$resultdata[0]['site_id'].' ', $array, $ret);

           if ($ret == 1) {
                $this->ajaxError("脚本未运行");
            }if ($ret == 2) {
                $this->ajaxError($array);
            }else {
               $list=json_decode($array[0],true);
               $html='';
               $title='';
               foreach ($list as $k=>$v){

                   switch ($k) {
                       case "orderNumber":
                           $title="快递单号";
                           break;
                       case "status":
                           $title="订单状态";
                           break;
                       case "company":
                           $title="快递公司";
                           break;
                       default:
                           $title="";
                           break;
                   }
                   $html.='<div class="layui-form-item">';
                   $html.='<label class="layui-form-label">'.$title.'</label>';
                   $html.='<div class="layui-input-block">';
                   $html.='<span>'.$v.'</span></div></div>';

               }
               $this->ajaxSuccess("成功", $html);
            }
        }
    }
    /**
     * @auth:leishaofa
     * @date:2017-07-26
     * @efect: 订单批量处理
    */
    public function batch(){
        if(IS_AJAX){
            $memberinfo = session('member_info');
        }

    }

}
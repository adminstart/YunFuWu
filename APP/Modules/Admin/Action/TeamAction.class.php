<?php
/**
 * Created by PhpStorm.
 * User: leishaofa
 * Date: 2017/7/9
 * Time: 15:37
 */
class TeamAction extends CommonAction{
    public function __construct(){
        parent::__construct();
        //import("rndChinaName",dirname(__FILE__).'/Extend/Vendor/',".class.php");
        //import("rndChinaName",dirname(__FILE__).'/Extend/Vendor/',".class.php");
        Vendor('rndChinaName','','.class.php');
    }

    /**
     * @auth:leishaofa
     * @date:204170709
     * @efect:管理员账号
     */
    public function memberlist(){
        //取分页数据
        $page=is_numeric($_GET['p'])?$_GET['p']:'';
        $model=M('','',C('flashSale'));
        $where='where a.m_type = 1 ';
        if($page) {
            $startnumber=($page-1)*C('PAGENUMBER');
            $listdata = $model->query(" select a.m_id as id ,a.m_name as name,a.m_email as email,a.m_status as is_status,a.m_updatetime as times, a.m_ip as ip ,c.t_name as teamName  from f_member as a left join f_member_rel as b ON a.m_id=b.m_id left join f_team as c on b.t_id=c.t_id ".$where." limit ".$startnumber.",".C('PAGENUMBER')." ");
            if (is_array($listdata) && !empty($listdata)) {
                return $this->ajaxReturn(array('code' => 101, 'msg' => "成功", 'data' => $listdata));
            } else {
                return $this->ajaxReturn(array('code' => 4001, 'msg' => "没有数据", 'data' =>[]));
            }
            exit;
        }
        /*
         * 获取团队列表
               $resultTeam=$model->query("select c.t_name,b.t_id from f_member as a left join f_member_rel as b ON a.m_id=b.m_id left join f_team as c on b.t_id=c.t_id WHERE a.m_type=1");
                $this->assign('team',$resultTeam);*/
        $resultcount=$model->query('select COUNT(*) as countnum from f_member as a '.$where);
        $this->assign('page',ceil($resultcount[0]['countnum']/C('PAGENUMBER')));
        C('TOKEN_ON',false);
        $this->display();
    }

    /**
     * @auth:leishaofa
     * @date:204170709
     * @efect:新增修改管理员
     * @parame:$_get['id']增加修改标示
     */
    public function add()
    {
        $id=is_numeric($_GET['id'])?$_GET['id']:'';
        $model = M('','',C('flashSale'));
        if(!empty($id)){
            //查询修改数据库
            $findsql="select a.*,c.* from f_member as a left join f_member_rel as b on b.m_id= a.m_id LEFT join f_team as c on b.t_id=c.t_id where a.m_id=".$id." limit 1";

            $update=$model->query($findsql);
        }
        if(IS_POST){
            $name = htmlspecialchars(trim($_POST['relname']));
            $teamname = htmlspecialchars(trim($_POST['teamname']));
            preg_match("/^$|^[^(and|or|exec|insert|select|union|update|&&|count|*|%)]*$/",$name,$relname);
            if(!$relname){
                parent::ajaxError("用户名包含非法字符");
            }
            preg_match("/^$|^[^(and|or|exec|insert|select|union|update|&&|count|*|%)]*$/",trim($teamname),$teamname);
            if(!$teamname){
                parent::ajaxError("团队名称包含非法字符");
            }
            if (!$model->autoCheckToken($_POST)){
                // 令牌验证错误
                parent::ajaxError('表单已提交过！请关闭窗口重新填写！');
            }
            if($id){
                if(empty(C('TeamDefaultPwd'))){
                    parent::ajaxError('请填写系统设置信息');
                }
                //修改
                $userstatus=isset($_POST['usersattus'])?1:2;
                $name = empty($relname[0]) ? $update[0]['m_email'] : $relname[0];
                $teamname = empty($teamname[0]) ? $update[0]['m_email'] : $teamname[0];
                $pwdreset=isset($_POST['pwdreset'])?md5(md5(C('TeamDefaultPwd')) . C('RAND_PASSWORD')):$update[0]['m_pwd'];
                $sql1="INSERT INTO f_member (m_id,m_name,m_status,m_pwd) VALUES(".$id.",'" . $name . "'," . $userstatus . ",'" .$pwdreset . "') ON DUPLICATE KEY UPDATE m_id=VALUES(m_id),m_name=VALUES(m_name),m_status=VALUES(m_status),m_pwd=VALUES(m_pwd) ";
                $result = M('member', 'f_', C('flashSale'))->execute($sql1);
                $result1 = M('team', 'f_', C('flashSale'))->execute("INSERT INTO f_team (t_id,t_name) VALUES(". $update[0]['t_id'].",'" . $teamname . "') ON DUPLICATE KEY UPDATE t_id=VALUES(t_id),t_name=VALUES(t_name)");
                if($result || $result1){
                    $this->ajaxSuccess('修改成功！');
                }else{
                    $this->ajaxError('修改失败');
                }
            }else {
                //添加
                $email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
                $password = htmlspecialchars($_POST['password']);
                $regPwd =  htmlspecialchars($_POST['regPwd']);
                $group_id =  is_numeric($_POST['jioase'])?$_POST['jioase']:'';
                if(empty($group_id)){
                    $this->ajaxError("请选择用户角色");
                }
                $data = M('member', 'f_', C('flashSale'))->query("select m_email from f_member where m_email ='" . $email . "' limit 1");
                if (!empty($data)) {
                    $this->ajaxError('该邮箱已经注册过！');
                }
                if ($password != $regPwd) {
                    $this->ajaxError('两次密码输入必须一致！');
                }
                if (strlen($password) < 6 || strlen($password) > 64) {
                    $this->ajaxError('密码长度太短或者密码太长');
                }
                $name = empty($relname[0]) ? $email : $relname[0];
                $teamname = empty($teamname[0]) ? $email : $teamname[0];
                $type = 1;
                switch ($type) {
                    case 1:  //团队
                        $class = 1;
                        break;
                    case 2:  //团队成员
                        $class = 2;
                        break;
                }
                $model->startTrans();
                $result = M('member', 'f_', C('flashSale'))->execute("INSERT INTO f_member (m_name,m_email,m_pwd,m_status,m_type) VALUES('" . $name . "','" . $email . "','" . md5(md5($password) . C('RAND_PASSWORD')) . "',2,1) ");
                $resultid = M('member', 'f_', C('flashSale'))->getLastInsId();
                $result1 = M('team', 'f_', C('flashSale'))->execute("INSERT INTO f_team (t_name) VALUES('" . $teamname . "')");
                $result1id = M('team', 'f_', C('flashSale'))->getLastInsId();
                $result2 = M('member_rel', 'f_', C('flashSale'))->execute("INSERT INTO f_member_rel (m_id,t_id,group_id) VALUES(" . $resultid . "," . $result1id . ",".$group_id.")");
                if ($result && $result1 && $result2) {
                    $model->commit();
                    $this->ajaxSuccess('新增成功！');
                } else {
                    $model->rollback();
                    $this->ajaxError('新增失败！' . $result . $result1 . $result2);
                }
            }
        }

        if(!empty($id)){
            $this->assign('updateone',$update[0]);
        }else{
            $auth_group_model = DX('AuthGroup',C('flashSale'));
            $listdata = $auth_group_model->query(" select group_id,group_name from f_group where is_enable=1 and  group_id!=1");
            $this->assign('jiaose',$listdata);
        }
        $this->display();//414e3a0fec160aef83cc58cae74babb9
    }

    /**
     * @auth:leishaofa
     * @date:204170710
     * @efect:团队任务列表
     */
    public function teamTask(){
        $model = M('', '', C('flashSale'));
        $where='';
        $where.=is_numeric($_GET['site'])?'a.site_id ='.$_GET['site']:'';
        $where.=is_numeric($_GET['status'])?'b.task_status ='.$_GET['status']:'';
        $where.=is_numeric($_GET['teamstatus'])?'a.status ='.$_GET['teamstatus']:'';
        $where.=empty(trim($_GET['teamname']))?'':'d.t_name linke "%'.htmlspecialchars($_GET['teamname']).'"';
        $where=empty(trim($where))?'':' where '.$where;
        if(IS_AJAX){
            $page=is_numeric($_GET['p'])?$_GET['p']:'';
            if(empty($page)){
                parent::ajaxError("页码错误");
            }
            $startnumber=($page-1)*C('PAGENUMBER');
            $sqlcount = "select a.*,b.task_status,b.task_url,c.site_name,c.site_status,d.t_name from f_task_rel as a left join f_task as b ON a.task_id=b.task_id and a.site_id=b.site_id left join f_site as c on a.site_id=c.site_id  LEFT JOIN f_team AS d ON a.t_id = d.t_id " . $where . " limit ".$startnumber." , ".C('PAGENUMBER')." ";
            $result = $model->query($sqlcount);
            if($result){
                $this->ajaxSuccess("成功",$result);
            }else{
                $this->ajaxError("失败");
            }



            exit;
        }
        $siteModel=M('site','f_',C('flashSale'));
        $sitesql="select site_id,site_name,site_status from f_site";
        $sitedao=$siteModel->query($sitesql);
        $this->assign('sitedao',$sitedao);
        $sqlcount = "select count(*) as counts from f_task_rel as a left join f_task as b ON a.task_id=b.task_id and a.site_id=b.site_id left join f_site as c on a.site_id=c.site_id " . $where . " ";
        $resultcount = $model->query($sqlcount);
        $this->assign('page',ceil($resultcount[0]['counts']/C('PAGENUMBER')));
        $this->display();
    }

    /**
     * @auth:leishaofa
     * @date:204170710
     * @efect:查询任务
     */
    public function searchtask(){
        if(IS_AJAX){
            $type=is_numeric($_POST['tyep'])?$_POST['tyep']:'';
            if(empty($type)){
                parent::ajaxError("请选择平台");
            }
            $url= filter_var(trim($_POST['goodsurl']), FILTER_VALIDATE_URL);
            if(!$url){
                parent::ajaxError("请输入正确的商品链接");
            }
            $model=M('','',C('flashSale'));
            $where='where b.site_status = 1 && a.task_status = 1 && a.site_id = '.$type.' && a.task_url like "'.$url.'%"';
            $sql="select a.task_url as url ,a.task_id as id ,a.goods_attr as attr  from f_task as a left join f_site as b ON a.site_id=b.site_id " .$where." order by a.task_id desc limit 20";
            $listdata = $model->query($sql);
            if(is_array($listdata)) {
                $listdata = array_map(function ($val) {
                    if ($val['attr'] != '0') {
                        $allattr = json_decode($val['attr'], true);
                        //循环商品属性
                        $val['attr'] = array_map(function ($v) {
                            return array_combine($v[1], $v[0]);
                        }, $allattr);
                        return $val;
                    } else {
                        return $val;
                    }
                }, $listdata);
                parent::ajaxSuccess("搜索成功",$listdata);
            }
            parent::ajaxError("查询无结果");
        }
    }

    /**
     * @auth:leishaofa
     * @date:204170710
     * @efect:团队用户
     */
    public function accountList(){
        $model = M('', '', C('flashSale'));
        $where='';
        /* $where.=is_numeric($_GET['site'])?'a.site_id ='.$_GET['site']:'';
         $where.=is_numeric($_GET['status'])?'b.task_status ='.$_GET['status']:'';
         $where.=is_numeric($_GET['teamstatus'])?'a.status ='.$_GET['teamstatus']:'';
         $where.=empty(trim($_GET['teamname']))?'':'d.t_name linke "%'.htmlspecialchars($_GET['teamname']).'"';
         $where=empty(trim($where))?'':' where '.$where;*/
        if(IS_AJAX){
            $page=is_numeric($_GET['p'])?$_GET['p']:'';
            if(empty($page)){
                parent::ajaxError("页码错误");
            }
            $startnumber=($page-1)*C('PAGENUMBER');
            $sqlcount = "select a.*,b.t_name as teamName from f_user as a left join f_team as b on a.t_id=b.t_id " . $where . " limit ".$startnumber." , ".C('PAGENUMBER')." ";
            $result = $model->query($sqlcount);
            $result=array_map(function($v){
                $v['u_site_no']=empty($v['u_site_no'])?'':explode(',',$v['u_site_no']);
                $v['u_site_ok']=empty($v['u_site_ok'])?'':explode(',',$v['u_site_ok']);
                $v['u_field']=json_decode($v['u_field'],true);
                $v['u_address']= $v['u_field']['addressremark']['country'].$v['u_field']['addressremark']['province'].$v['u_field']['addressremark']['city'].$v['u_field']['addressremark']['district'].$v['u_address'];
                $v['u_field']='';
                return $v;
            },$result);
            if($result){
                $this->ajaxSuccess("成功",$result);
            }else{
                $this->ajaxError("失败");
            }
            exit;
        }
        $sitesql = "select site_id as id,site_name as name from f_site where site_status = 1";
        $sitedao = $model->query($sitesql);
        $this->assign('sitelist',$sitedao);
        $siteModel=M('user','f_',C('flashSale'));
        $sqlcount = "select count(*) as counts from f_user as a left join f_team as b on a.t_id=b.t_id " . $where . " ";
        $resultcount = $model->query($sqlcount);
        $this->assign('page',ceil($resultcount[0]['counts']/C('PAGENUMBER')));
        $this->display();
    }

    /**
     * @auth:leishaofa
     * @date:2017-07-12
     * @efect:jsscript高德地图api定位
     */
    public function baidumap(){
        $this->assign('geokey',C('GEODEAK'));
        $this->display('baidugpsmap');
    }

    /**
     * @auth:leishaofa
     * @date:2017-07-13
     * @efect:发布注册任务
     */
    public function register(){
        if(IS_AJAX) {
            $memberinfo=session('member_info');
            $model = M('', '', C('flashSale'));;
            if (IS_POST && !empty($_POST['site'])) {
                $siteid=is_numeric($_POST['site'])?$_POST['site']:0;
                if(empty($siteid)){
                    $this->ajaxError("请选择平台");
                }
                $checksql="select site_id from f_site where site_status =1 and site_id = ".$siteid." ";
                $resultcheck = $model->query($checksql);
                if(is_array($resultcheck) && !empty($resultcheck)){
                    $addsql="INSERT INTO f_task_rel (task_id,t_id,rules,status,task_type,site_id) VALUES(0,".$memberinfo['t_id'].",'[]',1,1,".$resultcheck[0]['site_id'].") ";
                    $addresult =  $model->execute($addsql);
                    if($addresult){
                        parent::ajaxError($addsql);
                    }else{
                        parent::ajaxError($addsql);
                    }
                }else{
                    $this->ajaxError("该平台不存在");
                }
            }

            $sitesql = "select site_id,site_name from f_site where site_status = 1";
            $sitedao = $model->query($sitesql);
            $this->assign('sitedao', $sitedao);
            $this->display();
        }
    }


    /**
     * @auth:leishaofa
     * @date:204170718
     * @efect:随机生成批量用户
     */
    public function randomImport(){
        if(IS_AJAX){
            if (!empty($_POST)) {
                $memberinfo = session('member_info');
                if(!empty($memberinfo['m_type'])){
                    $this->ajaxError("没有权限");
                }
                //值接受过滤
                $postdata['exceltype']=is_numeric($_POST['exceltype'])?$_POST['exceltype']:'3';
                $postdata['userdefaultpwd']=preg_match("/^[a-zA-Z0-9]+$/", trim($_POST['pwd'])) && strlen(trim($_POST['pwd']))>6 ? trim($_POST['pwd']):C('defaultpwd');
                $postdata['agestarttime']=strtotime($_POST['agestarttime'])?date('Y-m-d',strtotime($_POST['agestarttime'])).' 00:00:00':C('starttime');
                $postdata['ageendtime']=strtotime($_POST['ageendtime'])?date('Y-m-d',strtotime($_POST['ageendtime'])).' 00:00:00':C('endtime');
                $postdata['gaodegps']=is_array(explode(",",$_POST['pgsmap']))?explode(",",$_POST['pgsmap']):'';
                $postdata['quankm']=is_numeric($_POST['quankm'])?$_POST['quankm']:C('quankm');
                $postdata['maxaccountnum']=is_numeric($_POST['maxaccountnum'])?$_POST['maxaccountnum']:0;
                if($postdata['maxaccountnum']>500 || $postdata['maxaccountnum']<0 || !floor($postdata['maxaccountnum'])==$postdata['maxaccountnum']){
                    $this->ajaxError("请填写正确的用户注册数量");
                }
                if(!$postdata['gaodegps'][0] || !$postdata['gaodegps'][1]){
                    $this->ajaxError("请点击获取收货地址以便于确认位置");
                }

                //高德坐标转换百度坐标
                $geodeexbaidu=bd_encrypt($postdata['gaodegps'][0],$postdata['gaodegps'][1]);
                //获取坐标地址信息
                $address=json_decode($this->getbaiduaddress($geodeexbaidu['bd_lat'],$geodeexbaidu['bd_lon']),true);
                $address=$address['result'];
                $u_city['country']=$address['addressComponent']['country'];
                $u_city['province']=$address['addressComponent']['province'];
                $u_city['city']=$address['addressComponent']['city'];
                $u_city['district']=$address['addressComponent']['district'];
                $u_city['street']=$address['addressComponent']['street'];
                $u_city['adcode']=$address['addressComponent']['adcode'];
                $u_city['formatted_address']=$address['formatted_address'];
                $u_city['sematic_description']=$address['sematic_description'];
                $postdata['addresscode']=$u_city['adcode']?$u_city['adcode']:$_POST['addresscode'];
                $remark = json_encode(['addressremark' => $u_city], JSON_UNESCAPED_UNICODE);
               $ponedata=$this->randomPone($postdata['maxaccountnum']);//生成手机号
                $insertvalues = '';
                //第一次poi百度地址
                $string=$geodeexbaidu['bd_lon'].",".$geodeexbaidu['bd_lat'];
                $searchpage=0;//搜索页数
                //更具坐标绘制矩形
                $resultpoicoordinate=$this->extendsbaidu($string,$postdata['quankm']);
                $searchkey=C('BAIDUSEARCHKEY')[array_rand(C('BAIDUSEARCHKEY'),1)];
                //更具矩形获取poi数据
               $resultaddressdata=$this->getbaidupoi($resultpoicoordinate,$searchkey,$searchpage);
                $oneadresstotle=0;//计数查询一次返回总数递减为0新赋值一个$searchkey且不能与当前相同;
                $cmulativeNumber=0;//加到20条请求下一页
                $oneadresstotle=$resultaddressdata['total'];
                if($resultaddressdata['status']==0){
                    $addressdata=$resultaddressdata['results'];//地址数据
                }else{
                    $this->ajaxError("定位地址参数出错");
                }
               $addresslist=[];
                $name_obj = new rndChinaName();
                foreach ($ponedata as $k=>$v){
                    //当计数大于该地址总和进入
                    if(!isset($addressdata[$cmulativeNumber])){
                        //请求下一页地址数据
                        $cmulativeNumber=0;
                        $searchpage++;
                        $resultaddressdata=$this->getbaidupoi($resultpoicoordinate,$searchkey,$searchpage);
                        $addressdata=$resultaddressdata['results'];
                        $oneadresstotle=$resultaddressdata['total'];
                        //地址数据为空或者总和为0重新赋值一个搜索词
                        if((20*$searchpage+$cmulativeNumber+1)>=$oneadresstotle || empty($addressdata) || $oneadresstotle==0){
                            $searchpage=0;//重置累计分页
                            $searchkeycacahe=C('BAIDUSEARCHKEY')[array_rand(C('BAIDUSEARCHKEY'),1)];//重置搜索词
                            $searchkey=$searchkeycacahe==$searchkey?C('BAIDUSEARCHKEY')[array_rand(C('BAIDUSEARCHKEY'),1)]:$searchkeycacahe;
                            $resultaddressdata=$this->getbaidupoi($resultpoicoordinate,$searchkey,$searchpage);//请求新的地址数据
                            $addressdata=$resultaddressdata['results'];//地址数据
                            $oneadresstotle=$resultaddressdata['total'];
                        }
                    }
                    $name = $name_obj->getName();
                    $accountinfo= $this->randomaccount($postdata['agestarttime'],$postdata['ageendtime'],$name);
                   $adress=str_replace(array($u_city['province'],$u_city['city'],$u_city['district'],"（","）"), '', $addressdata[$cmulativeNumber]['address'].$addressdata[$cmulativeNumber]['name']);

                    if (empty($insertvalues)) {
                        $insertvalues = "(" . $memberinfo['t_id'] . ",'" . $accountinfo['username'].$accountinfo['eamil'].C('EAMILSERVICES')[array_rand(C('EAMILSERVICES'),1)] . "','" . $postdata['userdefaultpwd'] . "','" . $v . "','" . $accountinfo['birthday'] . "','" . $accountinfo['username'].$accountinfo['eamil'] . "','" . $remark . "', '" . $name . "','".$u_city['province']."','". $postdata['addresscode']."','".$adress."')";
                    } else {
                        $insertvalues .= ",(" . $memberinfo['t_id'] . ",'" . $accountinfo['username'].$accountinfo['eamil'].C('EAMILSERVICES')[array_rand(C('EAMILSERVICES'),1)] . "','" . $postdata['userdefaultpwd'] . "','" . $v . "','" . $accountinfo['birthday'] . "','" . $accountinfo['username'].$accountinfo['eamil'] . "','" . $remark . "', '" . $name . "','".$u_city['province']."','". $postdata['addresscode']."','".$adress."')";
                    }
                    $cmulativeNumber++;
                }
            
                unset($ponedata,$accountinfo,$resultaddressdata,$addressdata,$name);
                $insertsql = "insert into f_user (t_id,u_eamil,u_pwd,mobile,birthday,u_name,u_field,firstname,u_city,u_addresscode,u_address) values " . $insertvalues . " ";
                unset($insertvalues);
                 $usermodel = M('user', 'f_', C('flashSale'));
                 $resultcount = $usermodel->execute($insertsql);
                 if($resultcount>0) {
                     $string = "成功导入" . $resultcount . "个用户";
                     $this->ajaxSuccess($string);
                 }else{
                     $string = "批量失败";
                     $this->ajaxError($string);
                 }
                exit;
            }
            $this->display();
            exit;
        }
        echo "非法请求";
    }

    /**
     * @auth:leishaofa
     * @date:2017-07-4
     * @efect:删除团队任务
     */
    public function deletaskrel(){
        if(IS_AJAX){
            $id=is_numeric($_POST['keyid'])?$_POST['keyid']:0;
            $memberinfo=session('member_info');
            if(empty($id)){
                parent::ajaxError("参数不存在");
            }
            // $sql="update f_task_rel set task_type=3 where task_relid=".$id." ";
            $sql="delete from f_task_rel where task_relid=".$id."  ";
            $taskmodel=M('task_rel','f_',C('flashSale'));
            if($taskmodel->execute($sql)){
                parent::ajaxSuccess("删除成功");
            }else{
                parent::ajaxError("删除失败");
            }
            exit;
        }
    }

    /**
     * @auth:leishaofa
     * @date:2017-10-10
     * @efect:批量修改用户，主要第一批生成的用户转移成第二种用户
    */
    public function saveuser(){
        if(IS_AJAX){
            $querysql="select u_id,u_site_no,u_site_ok,u_status from f_user ";//users where u_site_ok != null or u_site_no is not null";
            $model=M('','',C('flashSale'));
            $resultuser=$model->query($querysql);
           // $updatecalue
            foreach ($resultuser as $key=>$val){
                if(!empty($val['u_site_no']) || !empty($val['u_site_ok'])){
                    $u_site_no=empty($val['u_site_no'])?'':explode(',',$val['u_site_no']);
                    $u_site_ok=empty($val['u_site_ok'])?'':explode(',',$val['u_site_ok']);
                    $insertvalue='';
                    if(is_array($u_site_no)){
                        foreach ($u_site_no as $nv){
                            if(empty($insertvalue)){
                                $insertvalue ="('".$val['u_id']."','".$nv."','2','1')";
                            }else{
                                $insertvalue .=",('".$val['u_id']."','".$nv."','2','1')";
                            }
                        }

                    }
                    if(is_array($u_site_ok)){
                        foreach ($u_site_ok as $ov){
                            if(empty($insertvalue)){
                                $insertvalue ="('".$val['u_id']."','".$ov."','1','1')";
                            }else{
                                $insertvalue .=",('".$val['u_id']."','".$ov."','1','1')";
                            }
                        }
                    }
                    $model->execute( "insert into f_user_rel (u_id,site_id,u_site_status,u_site_task_status) values ".$insertvalue."; ");
                   // $model->execute( "update f_user set  u_site_no= '',u_site_ok = '' where u_id=".$val['u_id']."; ");
                }
            }
           //  $sql1="INSERT INTO f_user (u_id,u_site_no,u_site_ok) VALUES(".$id.",'" . $name . "'," . $userstatus . ",'" .$pwdreset . "') ON DUPLICATE KEY UPDATE m_id=VALUES(m_id),m_name=VALUES(m_name),m_status=VALUES(m_status),m_pwd=VALUES(m_pwd) ";
        }
    }

}
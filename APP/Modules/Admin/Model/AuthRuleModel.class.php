<?php
/**
 * Created by PhpStorm.
 * Author: leishaofa
 * Date: 2017/1/18 15:20
 */

class AuthRuleModel extends Model
{
    protected $trueTableName = 'f_action';

    /**
     * 根据规则id数组获取结算中心菜单
     * @author leishaofa
     * @param $rules_arr
     * @param int $is_menu 一二级菜单，1：一级菜单 2：二三级菜单
     * @return mixed
     */
    public function getMenu($rules_arr,$is_menu=0)
    {
        switch ($is_menu) {
            case 1:
                $where = array(
                    'act_id' => array('in', $rules_arr),
                    'act_pid' => array('eq', 0),
                    'act_type' => 1,
                );
                break;
            case 2:
                $where = array(
                    'act_id' => array('in', $rules_arr),
                    'act_pid' => array('neq', 0),
                    'act_type' => 1,
                );
                break;
            default:
                $where = array(
                    'act_id' => array('in', $rules_arr),
                    'act_type' => 1,
                );
                break;
        }
        return $this->where($where)->select();
    }

    /**
     * 根据规则id数组获取药企菜单
     * @param $rules_arr
     * @param int $is_menu
     * @return mixed
     */
    public function getDrugCompaniesMenu($rules_arr, $is_menu=0)
    {
        switch ($is_menu) {
            case 1:
                $where = array(
                    'permissions_id' => array('in', $rules_arr),
                    'p_permissions_id' => array('eq', 0),
                    'is_enable' => 1,
                    'is_display' => 1,
                    'action_type' => 1
                );
                break;
            case 2:
                $where = array(
                    'permissions_id' => array('in', $rules_arr),
                    'is_enable' => 1,
                    'is_display' => 1,
                    'action_type' => 1
                );
                break;
            default:
                $where = array(
                    'permissions_id' => array('in', $rules_arr),
                    'is_enable' => 1,
                    'is_display' => 1,
                    'action_type' => 1
                );
                break;
        }
        $field = ['permissions_id','permissions_name','permissions_icon','permissions_action','p_permissions_id','is_enable','is_display','action_type'];
        return $this->where($where)->field($field)->select();
    }

    /**
     * 根据规则id数组获取代表菜单
     * @param $rules_arr
     * @param int $is_menu
     * @return mixed
     */
    public function getDelegationMenu($rules_arr, $is_menu=0)
    {
        switch ($is_menu) {
            case 1:
                $where = array(
                    'permissions_id' => array('in', $rules_arr),
                    'p_permissions_id' => array('eq', 0),
                    'is_enable' => 1,
                    'is_display' => 1,
                    'action_type' => 2
                );
                break;
            case 2:
                $where = array(
                    'permissions_id' => array('in', $rules_arr),
                    'is_enable' => 1,
                    'is_display' => 1,
                    'action_type' => 2
                );
                break;
            default:
                $where = array(
                    'permissions_id' => array('in', $rules_arr),
                    'is_enable' => 1,
                    'is_display' => 1,
                    'action_type' => 2
                );
                break;
        }
        $field = ['permissions_id','permissions_name','permissions_icon','permissions_action','p_permissions_id','is_enable','is_display','action_type'];
        return $this->where($where)->field($field)->select();
    }

    /**
     * 显示权限
     * @param int $type ：模块名
     * @return mixed
     */
    public function selectAllAuth($type)
    {
        $where = array(
            'act_type'  => 1,
            'act_group' => $type
        );

       return $this->where($where)->select();
    }

    /**
     * 查询是否已存在的opt
     * @param $controller
     * @param $action
     * @param null $id
     * @return mixed
     */
    public function isExistOpt($controller, $action, $id=null)
    {
        $where = array(
            'act_action' => $controller.'/'.$action,
            'act_type'    => 1,

        );
        if($id){
            $where['permissions_id'] = array('neq',$id);
        }
        return $this->where($where)->find();
    }

    /**
     * 添加权限
     * @param $data
     * @param int $type 0：结算中心后台管理权限|1：药企结算管理权限|2：医药代表管理权限|3：经销商管理权限
     * @return mixed
     */
    public function addAuth($data)
    {
        $menu_info = array(
            'act_icon' => $data['icon'],
            'act_group' =>  $data['group'],
            'act_action' => !empty($data['controller']) ? trim($data['controller']).'/'.trim($data['action']) : '',
            'act_name'     => htmlspecialchars($data['menuname']),
            'act_pid'     => $data['pid'] ? $data['pid'] : 0,
            'is_display'  => $data['isdis']>1?2:1,
            'act_type'    =>  $data['act_type']?$data['act_type']:1
        );
        return $this->add($menu_info);
    }

    /**
     *修改权限
     * @param $data
     * @return bool
     */
    public function editAuth($data)
    {
        $where = array(
            'act_id' => $data['id'],
        );
        $menu_info = array(
            'act_icon' => $data['icon'],
            'act_action' => $data['controller'] ? $data['controller'].'/'.$data['action'] : '',
            'act_name'     => $data['menuname'],
        );
        unset($data['id']);
        return $this->where($where)->save($menu_info);
    }

    /**
     *根据id查询权限信息
     * @param $id
     * @return mixed
     */
    public function selectAuthById($id)
    {
        $where = array(
            'act_id' => $id,
        );
        $field = ["act_id,act_icon,act_pid,act_name,act_action,act_type"];
        return $this->where($where)->field($field)->find();
    }

    /**
     * 是否存在子权限
     * @param $id
     * @return mixed
     */
    public function isExistSonAuth($id)
    {
        $where = array(
            'act_pid' => $id,
            'act_type' => 1,
        );
        return $this->where($where)->find();
    }

    /**
     * 删除权限
     * @param $id
     * @return bool
     */
    public function deleteAuth($id)
    {
        $where = array(
            'act_id' => $id,
        );
        $data = array(
            'act_type' => 3,
        );

        return  $this->where($where)->save($data);
    }

}
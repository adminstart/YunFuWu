<?php

/**
 * 返回指定结果集用于接口方法
 * @param $code
 * @param $message
 * @param $data
 * @param $extra
 * @return json
 */
function responseJSON($code, $message, $data, $extra = []) {
    return json_encode(['code' => $code, 'message' => $message, 'data' => $data, 'extra' => $extra], JSON_UNESCAPED_UNICODE);
}

/**
 * 创建Token
 * @param $app_key
 * @param $app_secret
 * @return string
 */
function buildToken($app_key, $app_secret) {
    $data = $app_key . $app_secret
        . microtime(true) . rand(10000, 99999) . uniqid();
    return sha1($data) . rand(10000, 99999) . uniqid();
}

/**
 * 创建签名标识。
 *  算法规则：
 *      把传递的参数进行JSON化，并且组装成一个String。通过截取Token的第5位字符开始的10个字符。再次组装，通过MD5加密生成
 *      如果某人获取了SIGN,并进行下一次恶意调用，即传递重复的SIGN，当传递的是恶意的(不同)的参数，在服务端通过算法获取到的SIGN会和原本的SIGN
 *      不一致，则判定会恶意的签名调用。当然，由于某人不知道SIGN的算法。则无法伪造SIGN
 */
function createSign($data, $token) {
    return md5(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) . substr($token, 5, 10));
}

/**
 * 验证数据完整性操作。用于客户端在被劫持后的数据的完整性校验。严格的MD5加密算法
 * @param $data
 * @param $token
 * @param $md5Str
 * @return bool
 */
function authSign($data, $token, $md5Str) {
    if (createSign($data, $token) == $md5Str) {
        return true;
    } else {
        return false;
    }
}

/**
 * 分片ID
 * @return string
 */
function buildShardID() {
    $yCode = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j');
    $orderSn = $yCode[rand(0, 9)] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(10, 99)) . uniqid();
    return $orderSn;
}



/**
 * 正则匹配之手机号码匹配
 * @param $mobile
 * @return bool
 */
function mobileMatch($mobile) {
    if (preg_match(C('REGEX.mobile'), $mobile)) {
        return true;
    } else {
        return false;
    }
}

/**
 * 验证身份证号码
 * @param $idNumber
 * @return bool
 */
function idNumberMatch($idNumber) {
    if (preg_match(C('REGEX.idNumber'), $idNumber)) {
        return true;
    } else {
        return false;
    }
}

/*
 * 正则匹配密码长度
 * @param $pwd
 * @return bool
 */

function pwdMatch($pwd) {
    if (preg_match(C('REGEX.pwd'), $pwd)) {
        return true;
    } else {
        return false;
    }
}

/**
 * 解析文件后缀名
 *      eg
 *          1: xx/xxx/xx/xx.jpg             jpg
 *          2: xxx.ddd                      ddd
 *          3: xxx/xxx.d/xxx                ""
 * @param $file
 * @return mixed
 */
function parseFileExt($file) {
    if (($separator_pos = strrpos($file, DIRECTORY_SEPARATOR)) !== false) {
        $file = substr($file, $separator_pos + 1);
    }
    if (($point_pos = strrpos($file, ".")) !== false) {
        return substr($file, $point_pos + 1);
    }
    return "";
}



/**
 * description: 递归菜单 Mamicms的
 * @param unknown $array
 * @param number $fid
 * @param number $level
 * @param number $type 1:顺序菜单 2树状菜单
 * @return multitype:number
 */
function get_column($array, $type = 1, $fid = 0,$is_display=1, $level = 0) {
    $column = array();
    if ($type == 2) {
        foreach ($array as $k => $v) {
            if($is_display ==1 && $v['is_display'] == 1) {
                if ($v['act_pid'] == $fid) {
                    $v['level'] = $level;
                    $column[$k] = $v;
                    $column [$k][$v['act_id']] = get_column($array, 2, $v['act_id'],1, $level + 1);
                }
            }else{
                if ($v['act_pid'] == $fid) {
                    $v['level'] = $level;
                    $column[$k] = $v;
                    $column [$k][$v['act_id']] = get_column($array, 2, $v['act_id'],1, $level + 1);
                }
            }
        }
    } else {
        foreach ($array as $key => $vo) {
            // if ($vo['act_type'] == 1) {
            if ($vo['act_pid'] == $fid) {
                $vo['level'] = $level;
                $column[] = $vo;
                $column = array_merge($column, get_column($array, $type = 1, $vo['act_id'],1, $level + 1));
            }
            //   }
        }
    }
    return $column;
}

/**
 * 获取公网ip
 */

function getClientIp() {

    $onlineip = '';
    if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
        $onlineip = getenv('HTTP_CLIENT_IP');
    } elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
        $onlineip = getenv('HTTP_X_FORWARDED_FOR');
    } elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
        $onlineip = getenv('REMOTE_ADDR');
    } elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
        $onlineip = $_SERVER['REMOTE_ADDR'];
    }
    return $onlineip;

}

//GCJ-02(火星，高德) 坐标转换成 BD-09(百度) 坐标
//@param bd_lon 百度经度
//@param bd_lat 百度纬度
function bd_encrypt($gg_lon,$gg_lat)
{
    $x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    $x = $gg_lon;
    $y = $gg_lat;
    $z = sqrt($x * $x + $y * $y) - 0.00002 * sin($y * $x_pi);
    $theta = atan2($y, $x) - 0.000003 * cos($x * $x_pi);
    $data['bd_lon'] = $z * cos($theta) + 0.0065;
    $data['bd_lat'] = $z * sin($theta) + 0.006;
    return $data;
}
//BD-09(百度) 坐标转换成  GCJ-02(火星，高德) 坐标
//@param bd_lon 百度经度
//@param bd_lat 百度纬度
function bd_decrypt($bd_lon,$bd_lat)
{
    $x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    $x = $bd_lon - 0.0065;
    $y = $bd_lat - 0.006;
    $z = sqrt($x * $x + $y * $y) - 0.00002 * sin($y * $x_pi);
    $theta = atan2($y, $x) - 0.000003 * cos($x * $x_pi);
    $data['gg_lon'] = $z * cos($theta);
    $data['gg_lat'] = $z * sin($theta);
    return $data;
}

/**
 * @date:2017-07-12 9:23
 * @auth:网络搜索
 * @parame ：百度地图php版调用方法
 */
function caculateAKSN($ak, $sk, $url, $querystring_arrays, $method = 'GET')
{
    if ($method === 'POST'){
        ksort($querystring_arrays);
    }
    $querystring = http_build_query($querystring_arrays);
    return md5(urlencode($url.'?'.$querystring.$sk));
}

/**
 * @date:2017-07-12 10:57
 * @auth:百度地图文档
 * 百度坐标系转换成标准GPS坐系
 * @param float $lnglat 坐标(如:106.426, 29.553404)
 * @return string 转换后的标准GPS值:
 */
function BD09LLtoWGS84($lnglat){ // 经度,纬度
    $lnglat = explode(',', $lnglat);
    list($x,$y) = $lnglat;
    $Baidu_Server = "http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x={$x}&y={$y}";
    $result = @file_get_contents($Baidu_Server);
    $json = json_decode($result);
    if($json->error == 0){
        $bx = base64_decode($json->x);
        $by = base64_decode($json->y);
        $GPS_x = 2 * $x - $bx;
        $GPS_y = 2 * $y - $by;
        return $GPS_x.','.$GPS_y;//经度,纬度
    }else
        return $lnglat;
}

/**
 * @date:2017-07-12 11:34
 * @auth:leishaofa
 *@parame根据gps一个坐标绘制一个矩形
 * @parame$lng 经度
 * @parame$lat 纬度
 * @parame $distance一千米
 */
function resultmap($lng,$lat,$distance=1){
    $fEARTH_RADIUS=6371;//地球平均半径
    $dlng=2*asin(sin($distance / (2*$fEARTH_RADIUS))/cos(deg2rad($lat)));
    $dlng=rad2deg($dlng);
    $dlat=$distance/$fEARTH_RADIUS;
    $dlat=rad2deg($dlat);
    return array(
        'left-top'=>array('lat'=>$lat+$dlng,'lng'=>$lng-$dlng),
        'right-top'=>array('lat'=>$lat+$dlng,'lng'=>$lng+$dlng),
        'left-bottom'=>array('lat'=>$lat-$dlng,'lng'=>$lng-$dlng),
        'right-bottom'=>array('lat'=>$lat-$dlng,'lng'=>$lng+$dlng)
    );
}

/**
 * mamicms 的类别树状结构
 * @param $arr
 * @param int $pid
 * @return array
 */
function arrayToTree($arr, $pid = 0) {
    $ret = [];
    foreach ($arr as $k => $v) {
        if ($v['pid'] == $pid) {
            $tmp = $arr[$k];
            unset($arr[$k]);
            $tmp['children'] = arrayToTree($arr, $v['cat_id']);
            $ret[] = $tmp;
        }
    }

    return $ret;
}

/**
 * 生成随机数
 * @return array
 */
function sugar() {
    $numbers = range(1, 99999);
    shuffle($numbers);
    $num = 6;
    $result = array_slice($numbers, 0, $num);
    $sugar = array_sum($result);
    return $sugar;
}

function generatorID()
{
    $yCode = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j');
    $orderSn = $yCode[rand(0,9)] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(10, 99)).uniqid();
    return $orderSn;
}

/**
 * 生产计划任务号
 */
function getorderid(){
    return numLatter(intval(date('y'))).numLatter(date("G")).numLatter(rand(1,9)).strtoupper(dechex(date('m'))).numLatter(substr(microtime(),2,4)).date('d').date("i").date("s"). sprintf('%02d', rand(0, 9999));
}

function numLatter($number){
    $yCode = array('a','b', 'c', 'd', 'e', 'f', 'f', 'h', 'i', 'j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
    $nums_arr=explode("\r\n",chunk_split($number,1));
    $str='';
    foreach($nums_arr as $n){
        if($n!=''){
            $str.=$yCode[$n];
        }
    }
    return $str;
}

function getbrowser() {
    global $_SERVER;
    $agent  = $_SERVER['HTTP_USER_AGENT'];
    $browser  = '';
    $browser_ver  = '';

    if (preg_match('/OmniWeb\/(v*)([^\s|;]+)/i', $agent, $regs)) {
        $browser  = 'OmniWeb';
        $browser_ver   = $regs[2];
        if(intval($browser_ver)<6){
            echo '需要OmniWeb6版本，虽然5.8也可用但是放弃';
            exit;
        }
    }

    if (preg_match('/Netscape([\d]*)\/([^\s]+)/i', $agent, $regs)) {
        $browser  = 'Netscape';
        $browser_ver   = $regs[2];
        if(intval($browser_ver)<6){
            echo 'Netscape是什么鬼浏览器，可能太少人用了吧！如果你是苹果系统，可以尝试使用OmniWeb6或者Safari！';
            exit;
        }
    }

    if (preg_match('/safari\/([^\s]+)/i', $agent, $regs)) {
        $browser  = 'Safari';
        $browser_ver   = $regs[1];
    }

    if (preg_match('/MSIE\s([^\s|;]+)/i', $agent, $regs)) {
        $browser  = 'Internet Explorer';
        $browser_ver   = $regs[1];
        if(intval($browser_ver)<10){
            echo '请使用ie10以上版本';
            exit;
        }

    }

    if (preg_match('/Opera[\s|\/]([^\s]+)/i', $agent, $regs)) {
        $browser  = 'Opera';
        $browser_ver   = $regs[1];
        if(intval($browser_ver)<40){
            echo '建议使用Opera40以上版本';
            exit;
        }
    }

    if (preg_match('/NetCaptor\s([^\s|;]+)/i', $agent, $regs)) {
        $browser  = '(Internet Explorer ' .$browser_ver. ') NetCaptor';
        $browser_ver   = $regs[1];
        if(intval($browser_ver)<7){
            echo '建议使用7.0以上版本';
            exit;
        }
    }

    if (preg_match('/Maxthon/i', $agent, $regs)) {
        $browser  = '(Internet Explorer ' .$browser_ver. ') Maxthon傲游';
        $browser_ver   = '';

    }
    if (preg_match('/360SE/i', $agent, $regs)) {
        $browser       = '(Internet Explorer ' .$browser_ver. ') 360SE';
        $browser_ver   ='';
    }
    if (preg_match('/QQBrowser/i', $agent, $regs)){
        $browser       = 'QQ浏览器';
        $browser_ver   ='';
    }
    if (preg_match('/SE 2.x/i', $agent, $regs)) {
        $browser       = '(Internet Explorer ' .$browser_ver. ') 搜狗';
        $browser_ver   = $regs[1];
        if(intval($browser_ver)<7){
            echo '建议使用7.0或者最新版因为我不能保证功能完整性';
            exit;
        }
    }

    if (preg_match('/FireFox\/([^\s]+)/i', $agent, $regs)) {
        $browser  = 'FireFox';
        $browser_ver   = $regs[1];
        if(intval($browser_ver)<48){
            echo "请使用50以上版本防止有些功能不能正常使用";
            exit;
        }
    }

    if (preg_match('/Lynx\/([^\s]+)/i', $agent, $regs)) {
        $browser  = 'Lynx';
        $browser_ver   = $regs[1];
        echo '还不支持Lynx浏览器';
        exit;
    }

    if(preg_match('/Chrome\/([^\s]+)/i', $agent, $regs)){
        $browser  = 'Chrome';
        $browser_ver   = $regs[1];
        if(intval($browser_ver)<44){
            echo '安全考虑请使用Google50以上版本';
            exit;
        }
    }

    if ($browser != '') {
        // return ['browser'=>$browser,'version'=>$browser_ver];
    } else {
        echo '如果不支持您的浏览器反馈我们工程师测试之后会发布上去的';
        exit;
        // return ['browser'=>'unknow browser','version'=>'unknow browser version'];
    }

}

/**
 * DX函数用于实例化Model 格式 项目://分组/模块
 * @param string $name Model资源地址
 * @param string $layer 业务层名称
 * @parame string $db  数据库连接信息
 * @return Model
 */
function DX($name='',$db,$layer='') {
    if(empty($name)) return new Model;
    static $_model  =   array();
    $layer          =   $layer?$layer:C('DEFAULT_M_LAYER');
    if(strpos($name,'://')) {// 指定项目
        list($app)  =   explode('://',$name);
        $name       =   str_replace('://','/'.$layer.'/',$name);
    }else{
        $app        =   C('DEFAULT_APP');
        $name       =   $app.'/'.$layer.'/'.$name;
    }
    if(isset($_model[$name]))   return $_model[$name];
    $path           =   explode('/',$name);
    if($list = C('EXTEND_GROUP_LIST') && isset($list[$app])){ // 扩展分组
        $baseUrl    =   $list[$app];
        import($path[2].'/'.$path[1].'/'.$path[3].$layer,$baseUrl);
    }elseif(count($path)>3 && 1 == C('APP_GROUP_MODE')) { // 独立分组
        $baseUrl    =   $path[0]== '@' ? dirname(BASE_LIB_PATH) : APP_PATH.'../'.$path[0].'/'.C('APP_GROUP_PATH').'/';
        import($path[2].'/'.$path[1].'/'.$path[3].$layer,$baseUrl);
    }else{
        import($name.$layer);
    }
    $class          =   basename($name.$layer);
    if(class_exists($class)) {
        $model      =   new $class(basename($name),'',$db);
    }else {
        $model      =   new Model(basename($name),'',$db);
    }
    $_model[$name]  =  $model;
    return $model;
}

//获取url地址
function getUrl(){
    $parameter = $_GET; //获取url action的参数
    if(is_array($parameter)){
        $parameterNum='';
        foreach ($parameter as $key=>$val){
            switch ($key) {
                case 'type':    //$key = 'type' 的时候需要拼接action参数
                    $parameterNum .= '/' . $key . '/' . $val;
                    break;
                default:
                    return MODULE_NAME . '/' . ACTION_NAME;
            }
        }
    }
    return $parameterNum ? MODULE_NAME . '/' . ACTION_NAME . $parameterNum : MODULE_NAME . '/' . ACTION_NAME;
}



function nextMobile()
{
    $prefix = C('MODELPONE')[array_rand(C('MODELPONE'))];
    $middle = mt_rand(2000, 9000);
    $suffix = mt_rand(2000, 9000);

    return $prefix . $middle . $suffix;
}

/**
 * @auth:leishaofa
 * @data:20170823
 * @efect:固定服务商对应的服务器区域和机型
 * @parame:$type(服务商标示);
*/
function getdiscribedata($type){
    switch ($type) {
        case 1:
            $resultdata = array(
                '华北1' => 'Beijing',
                '华东1' => 'EastChina1'
                );
            break;
        case 2:
            //区域:https://cloud.tencent.com/document/product/213/6091
            //机型：https://cloud.tencent.com/document/product/213/2177
            $resultdata = array(
                '广州二区' => array('ap-guangzhou-2:S2' => '标准型S2', 'ap-guangzhou-2:S1' => '上一代标准型S1'),
                '广州三区' => array('ap-guangzhou-3:S2' => '标准型S2', 'ap-guangzhou-3:S1' => '上一代标准型S1'),
                '上海一区' => array('ap-shanghai-1:S2' => '标准型S2', 'ap-shanghai-1:S1' => '上一代标准型S1'),
                '北京二区' => array('ap-beijing-2:S2' => '标准型S2'),
                '北京一区' => array('ap-beijing-1:S1' => '上一代标准型S1'));
            break;
        case 4:
            //区域:https://help.aliyun.com/document_detail/40654.html?spm=5176.doc25499.2.7.mGR51Y
            //机型：https://help.aliyun.com/document_detail/25378.html?spm=5176.doc25499.2.8.jgrsbz
            //费用说明：https://help.aliyun.com/knowledge_detail/40653.html
            $resultdata = array(
                '青岛' => array('cn-qingdao:ecs.sn2ne' => '网络增强型sn2ne', 'cn-qingdao:ecs.sn1ne' => '网络增强型sn1ne', 'cn-qingdao:ecs.XN4' => '入门级XN4', 'cn-qingdao:ecs.N4' => '入门级N4', 'cn-qingdao:ecs.g5' => '标准型g5', 'cn-qingdao:ecs.c5' => '标准型c5'),
                '北京' => array('cn-beijing:ecs.sn2ne' => '网络增强型sn2ne', 'cn-beijing:ecs.sn1ne' => '网络增强型sn1ne', 'cn-beijing:ecs.XN4' => '入门级XN4', 'cn-beijing:ecs.N4' => '入门级N4', 'cn-beijing:ecs.g5' => '标准型g5', 'cn-beijing:ecs.c5' => '标准型c5'),
                '张家口' => array('cn-zhangjiakou:ecs.sn2ne' => '网络增强型sn2ne', 'cn-zhangjiakou:ecs.sn1ne' => '网络增强型sn1ne', 'cn-zhangjiakou:ecs.XN4' => '入门级XN4', 'cn-zhangjiakou:ecs.N4' => '入门级N4', 'cn-zhangjiakou:g5' => '标准型g5', 'cn-zhangjiakou:ecs.c5' => '标准型c5'),
                '杭州' => array('cn-hangzhou:ecs.sn2ne' => '网络增强型sn2ne', 'cn-hangzhou:ecs.sn1ne' => '网络增强型sn1ne', 'cn-hangzhou:ecs.XN4' => '入门级XN4', 'cn-hangzhou:ecs.N4' => '入门级N4', 'cn-hangzhou:ecs.g5' => '标准型g5', 'cn-hangzhou:ecs.c5' => '标准型c5'),
                '上海' => array('cn-shanghai:ecs.sn2ne' => '网络增强型sn2ne', 'cn-shanghai:ecs.sn1ne' => '网络增强型sn1ne', 'cn-shanghai:ecs.XN4' => '入门级XN4', 'cn-shanghai:ecs.N4' => '入门级 N4', 'cn-shanghai:ecs.g5' => '标准型g5', 'cn-shanghai:ecs.c5' => '标准型c5'));
            break;
        default:
            $resultdata = array();
            break;

    }
    return $resultdata;
}


/**
 * @auth:leishaofa
 * @data:20170823
 * @efect:获取服务器地域中文名称
 * @parame:$name(机房标牌)，$sertype(服务商标示)
 */
function getservercity($name,$sertype){
    $zone=explode('-',$name);
    $countzone=count($zone);
    if($countzone==1){
        return "未知";
    }
    $name='未知';
    $result=array_keys(getdiscribedata($sertype));
    vendor('pinyin.src.Pinyin');
    $pinyin=new Overtrue\Pinyin\Pinyin();
    foreach ($result as $key=>$val){
        $cachedata=$pinyin->convert($val);
        if(in_array('qu',$cachedata)){
            unset($cachedata[count($cachedata)-1]);
        }

        $number=is_numeric($cachedata[count($cachedata)-1])?$cachedata[count($cachedata)-1]:0;
        $cachedata=implode($cachedata,'');
        $cachedata=preg_replace('|[0-9]+|','',$cachedata);
        if($countzone == 3 && $number == $zone[2] && $cachedata == $zone[1]){
                $name=$val;
        }elseif ($countzone == 2 && $cachedata == $zone[1]){
                $name=$val;
        }
    }
    return $name;

}


/**
 * @auth:leishaofa
 * @date:2017803
 * @efect:私有方法美团云调用方法
 * @parame:$data:服务商信息     array()
 * @parame:$actiondata :调用方法以及方法参数   array()
 */
function apimeituanyun($data,$actiondata){
    $key = $data['ser_key'];//key
    $secret = $data['ser_secret'];//secret
    $posturl=trim($data['ser_url']);//api地址
    $signurl=parse_url($posturl);//拆分后的api地址
    $time=str_replace('+','.',date(DATE_ISO8601))."Z";
    $data=array('Format'=>'json','AWSAccessKeyId'=>$key,'SignatureVersion'=>2,'Timestamp'=>$time,'Region'=>empty($data['Region'])?'NorthChina1':$data['Region'],'SignatureMethod'=>HmacSHA256);//固定请求参数
    $data=array_merge($data,$actiondata);//合并组装所有请求参数
    ksort($data);//参数名称的字典顺序排序（重小到大）
    $strdata=http_build_query($data);//x-www-form-urlencoded数据组装
    $hashdata="POST\n".$signurl['host']."\n".$signurl['path']."\n".$strdata;//组装签名内容
    $sign= base64_encode( hash_hmac('sha256', $hashdata, $secret,true));//获取签名以HmacSHA256 算法进行Hash后，进行base64编码
    $data['Signature']=$sign;
    $postData = http_build_query($data);//x-www-form-urlencoded数据组装
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $posturl);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
    curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);//允许重定向吧
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));//x-www-form-urlencoded编码
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData); // Post提交的数据包
    $data = json_decode(curl_exec($curl),true);
    curl_close($curl);
    return $data;
}

/**
 * @auth:leishaofa
 * @date:2017803
 * @efect:腾讯
 * @parame:$data:服务商信息     array()
 * @parame:$actiondata :调用方法以及方法参数   array()
 */
function tencat($server,$action,$actiondata){
    Vendor('QcloudApi.QcloudApi');
    $config = array(
        'SecretId'      => $server['ser_key'],
        'SecretKey'     => $server['ser_secret'],
        'RequestMethod'  => 'GET',
        'DefaultRegion' => empty($server['Region'])?'gz':$server['Region']
    );
    $cvm = QcloudApi::load($server['model'], $config);
    $package = array('SignatureMethod' =>'HmacSHA256');
    $package=array_merge($package,$actiondata);//合并组装所有请求参数
    ksort($package);
    $a=$cvm->$action($package);
    return  json_decode($cvm->getLastResponse(),true);
}


/**
 * @auth:leishaofa
 * @date:2017818
 * @efect:私有方法阿里云调用方法
 * @parame:$data:服务商信息     array()
 * @parame:$actiondata :调用方法以及方法参数   array()
 */
function ali($server,$action,$actiondata){
    Vendor('AliyunAPI.aliyun-php-sdk-core.Config');
    $clientProfile =DefaultProfile::getProfile($server['zone'], $server['ser_key'],$server['ser_secret']);
    $client = new DefaultAcsClient($clientProfile);
    $request = new $action();
    if(is_array($actiondata) && !empty($actiondata)){
        foreach ($actiondata as $key=>$val){
            $request->$key($val);
        }
    }# 发起请求并处理返回
    try {
        $response = $client->getAcsResponse($request);
        return $response;
    } catch(ServerException $e) {
        return "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
    } catch(ClientException $e) {
        return "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
    }
}

function checkIdCard($idcard){

    // 仅仅能是18位
    if(strlen($idcard)!=18){
        return false;
    }

    // 取出本体码
    $idcard_base = substr($idcard, 0, 17);

    // 取出校验码
    $verify_code = substr($idcard, 17, 1);

    // 加权因子
    $factor = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);

    // 校验码相应值
    $verify_code_list = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');

    // 依据前17位计算校验码
    $total = 0;
    for($i=0; $i<17; $i++){
        $total += substr($idcard_base, $i, 1)*$factor[$i];
    }

    // 取模
    $mod = $total % 11;

    // 比較校验码
    if($verify_code == $verify_code_list[$mod]){
        return true;
    }else{
        return false;
    }

}



# -*- coding: utf-8 -*-
import requests
import sys
import urllib
import json
from bs4 import BeautifulSoup
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')
import warnings
warnings.filterwarnings("ignore")

"""
功能:支付链接获取支付二维码图片链接,返回给php
"""

def getPay(url,website):

	loginHead={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
		 		   'Referer': 'https://www.adidas.com.cn/customer/account/login/'}
	html=requests.get(url,headers=loginHead,verify=False)
	zhifusoup=BeautifulSoup(html.text,"html.parser")
	if website == '1':
		payurl = zhifusoup.select('#J_qrImgUrl')[0]['value']
		return payurl
	if website == '3':
		payurl = zhifusoup.select('input')[4]['value']
		return payurl
	if website == '2':
		# payurl = zhifusoup.select('#J_qrImgUrl')[0]['value'] #支付阿宝
		return url #微信支付
	else:
		loginHead={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
			 		   'Referer': 'https://www.adidas.com.cn/customer/account/login/'}
		html=requests.get(url,headers=loginHead,verify=False)
		zhifusoup=BeautifulSoup(html.text,"html.parser")
		if website == '1': #阿迪达斯支付宝支付
			payurl = zhifusoup.select('#J_qrImgUrl')[0]['value']
			return payurl 
		if website == '3': #耐克支付宝支付
			payurl = zhifusoup.select('#J_qrImgUrl')[0]['value']
			return payurl
		

if __name__ == '__main__':

	print getPay(str(sys.argv[1]),str(sys.argv[2]))







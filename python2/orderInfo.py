# -*- coding: utf-8 -*-
import requests
import json
from bs4 import BeautifulSoup
import sys
import urllib
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')
import warnings
warnings.filterwarnings("ignore")

"""
功能：获取物流信息，订单状态，物流链接，运输公司
"""

def nike(data):
	session=requests.Session()
	url='https://secure-store.nike.com/nikestore/html/services/orders/orderDetails'
	head={'referer':'https://secure-store.nike.com/common/content/endpoint.html',
		  'content-type':'application/x-www-form-urlencoded',
		  'x-requested-with':'XMLHttpRequest',
		  'user-agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36'}
	#session.get(url,headers=head,verify=False)#获取页面cookies
	data=urllib.urlencode(data)
	orderInfo=session.post(url,data=data,headers=head,verify=False)
	

	trackingUrl=json.loads(orderInfo.text)['data']['order']['shippingGroups'][0]['trackingUrl']
	trackingNumber=json.loads(orderInfo.text)['data']['order']['shippingGroups'][0]['trackingNumber']
	status=json.loads(orderInfo.text)['data']['order']['shippingGroups'][0]['status']
	return {'trackingUrl':trackingUrl,'status':status,'trackingNumber':trackingNumber}


def adidas(data):
	session = requests.Session()
	queryUrl = 'https://www.adidas.com.cn/guest/checkGuestOrderAvailable.json?guestOrder='+data['orderId']+'&guestMobilePhone='+data['mobile']+'&_=1503208348415'
	queryHead = {   'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
					'Accept-Encoding':'gzip, deflate, sdch',
					'Accept-Language':'zh-CN,zh;q=0.8',
					'Connection':'keep-alive',
					'Host':'www.adidas.com.cn',
					'Referer':'https://www.adidas.com.cn/orderGuest',
					'Upgrade-Insecure-Requests':'1',
					'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36'}
	queryResp = session.get(queryUrl,headers=queryHead,verify=False)
	# print queryResp.url 
	# print queryResp.text
	url = 'https://www.adidas.com.cn/guestorderquery/'+data['orderId']
	queryResp = session.get(url,headers=queryHead,verify=False)
	#print queryResp.text
	soup = BeautifulSoup(queryResp.text,'html.parser')
	orderStatus = soup.select('.order-info strong')[0].text.encode('utf-8')
	orderNumber=str(soup.select('.info-wrapper p span')[2].text).split('订单编号： ')[1]
	print orderStatus,orderNumber
	if orderStatus == '待支付':
		print '暂未生成物流信息'
		return {'company':'','orderNumber':orderNumber,'status':orderStatus}
	else:
		return {'company':'','orderNumber':orderNumber,'status':orderStatus}
	

def apple():
	session = requests.Session()

	#取得订单页面
	orderPage_Url = 'https://secure2.store.apple.com/cn/order/list'
	orderPageHead = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
				 'Referer':'https://secure2.store.apple.com/cn'}

	orderPage_Resp = session.get(orderPage_Url,headers=orderPageHead,verify=False)

	# 输入订单信息
	orderUel = 'https://secure2.store.apple.com/cn/shop/order/json/single'
	orderHead = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
				 'Referer':'https://secure2.store.apple.com/cn',
				 'Content-Type':'application/x-www-form-urlencoded',
				 'X-Requested-With':'XMLHttpRequest'}

	data =urllib.urlencode( {'orderLookup-order-number':str(sys.argv[1]),
			'orderLookup-order-user-info':str(sys.argv[2]),
			'_a':'login.lookup',
			'_fid':'lu'} )

	orderResp = session.post(orderUel,headers=orderHead,data=data,verify=False)
	#print orderResp.text

	#获取信息链接
	infoUrl = json.loads(orderResp.text)['head']['data']['url']
	#params = {'e':'true'}
	get_OrderResp = session.get(infoUrl,headers=orderPageHead)
	#print get_OrderResp.text
	soup = BeautifulSoup(get_OrderResp.text,'html.parser')
	orderStatus = soup.select('.h3 span')[0].text
	return orderStatus





if __name__ == '__main__':


	website=str(sys.argv[3])
	#website = '3'

	#平台选择
	if website=='3': 
		# data={'oar_email':str(sys.argv[2]), 'oar_order_id':str(sys.argv[1])}

		data = {'action':'getAnonymousOrderDetails',
				'lang_locale':'zh_CN',
				'country':'CN',
				'endpoint':'getAnonymousOrderDetails',
				'orderId':str(sys.argv[1]),
				'email':str(sys.argv[2]),
				'deviceType':'desktop'}
		#data={'oar_email':'1774507011@qq.com', 'oar_order_id':'C00012819207'}
		print json.dumps(nike(data))

	if website=='1':
		data={  'orderId':str(sys.argv[1]),
				'mobile':str(sys.argv[3])}
				
		print json.dumps(adidas(data))

	if website == '2':
		print json.dumps(apple(sys.argv[1],sys.argv[2]))
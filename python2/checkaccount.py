# -*- coding:utf-8 -*-

import requests
import sys
import json
import warnings
warnings.filterwarnings("ignore")

def login(email,password):
	loginUrl = 'https://unite.nike.com/loginWithSetCookie?appVersion=315&experienceVersion=276&uxid=com.nike.commerce.snkrs.web&locale=zh_CN&backendEnvironment=identity&browser=Google%20Inc.&os=undefined&mobile=false&native=false&visit=2&visitor=1ffde67e-a737-4b78-8efd-465e567ea05c'
	#loginUrl='https://unite.nikecloud.com/login?locale=ch_CN&backendEnvironment=identity'
	data={  'client_id': "PbCREuPr3iaFANEDjtiEzXooFl7mXGQ7",
			'grant_type': "password",
			'password': password,
			'username': email,
			'ux_id': "com.nike.commerce.snkrs.web"}

	head={'user-agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',
		'content-type':'text/plain'
		}

	loginResp=requests.post(loginUrl,headers=head,data=json.dumps(data),verify=False)
	if loginResp.status_code == 200:
		status_code = '1'
	else:
		status_code = '0'

	return status_code


if __name__ == '__main__':

	email = sys.argv[1]
	password = sys.argv[2]
	print login(email,password)
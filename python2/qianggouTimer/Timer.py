# -*-coding: utf-8 -*-

from crontab import CronTab
import time
import sys
 
def Create_task(timer,filename):

	'''创建一个定时任务'''

	# 创建当前用户的crontab
	my_user_cron  = CronTab(user=True)
	 
	# 创建任务
	job = my_user_cron.new(command='python /home/qianggou/www/system/python' + filename)
	
	#设置名称(就是日期时间)
	job.set_comment(timeinfo)
	 
	# 设置任务执行周期
	job.setall(timer)

	#写入配置文件
	my_user_cron.write()

def Remove_task(name):

	'''删除一个定时任务'''

	# 获取所以crontab任务
	cron = CronTab(user=True)
	
	#删除任务
	cron.remove_all(comment=name)
	cron.write()


# 开启任务
filename = str(sys.argv[1])
timeinfo = sys.argv[2]

#时间处理成crontab格式
now_time = time.time()
target_time = time.mktime(time.strptime(timeinfo, "%Y-%m-%d %H:%M:%S"))

crontime = str ((target_time-now_time)/3600)

timer = '* */'+ crontime +'* * *'

#创建任务
Create_task(timer,filename)



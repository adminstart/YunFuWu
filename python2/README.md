# 模块
- 商品抢购+填写收货信息+生成订单+提取付款链接模块
- 查询订单信息模块


# 待完成
## 耐克
- php调用商品尺码信息模块测试
- 耐克抢购模块调用php接口
-  php调用订单信息模块测试

# 流程
拉任务—判断有无任务-打开商品链接—判断是否能打开商品链接-抢购-判断是否抢购成功-拉用户-判断有无用户数据-下订单

# 问题 
- 耐克订单没记录
- 获取任务状态失败

# 问题
- 收货地址太长添加失败（解决）->地址内容不能超过35个

- 商品售馨取不到添加购物车信息(待售和售馨没有加入购物车按钮，阿迪达斯耐克都是靠这个判断)

- 收货人姓名乱码(解决)->(要<type 'unicode'>类 宋永怡 ascii不行)

- 可以加入购物车但可能出现一下错误，程序在下订单时候需要判断一下（解决）
nike.ocp.serverErrors = ['抱歉，操作超时，您的购物车里低库存商品：Nike Dunk Flyknit 男子运动鞋，尺码47.5仅剩白色/狼灰/黑色已不存在，可尝试再次添加。 [错误代码: 031A-05200023]']

- 极少数情况加载不到支付链接，判断有无支付链接在获取
-出错 [错误代码: 007I-05200023] 

# 抢购速度

- adidas 19.7s/单
- nike 5.13s/单

# 问题
- 未知错误
- 收货地址乱码（沿河大道236附14号江山如�
��-2期）

# apple
## 重要元素：机型，外观，容量
## 加入购物车

# sessionID
23622711023f079b09b2acbebb35b7ca205543679fa6f31cfaa2616630c0a982
4cd4f110d12775e3ba42c5922a1e1e9074574a641870fbddda044f10376554f1

https://secure1.store.apple.com/cn/shop/checkout
https://secure3.store.apple.com/cn/shop/checkout

# 问题
- 修改nike抢购程序
- 读取tid拉任务时传php
- 完成苹果抢购程序


849CA0DC9B5A93A330CA33186941195EB83257A240270000381C8C59E3B65C37~plVOlutS0LtBolN4BBhcQ6Qda6QcavWYygcFf/w039wqyep14tgNSRyeYWbfcgXSd+p9G64iyvdz43+t+fOXLd3kUThZukJmj41imsvrHLe6Uxm4CHz+kCINBdbjdYvjW0Z9T8kfg34lgIeMEwm6Pu+1RSflACoV/6vXoajzOlC9noCCFoTVnx/3pCfIx4iTZvLGX1FKq/m3eB38ZB7gkEISGjzjDyy0Q/Xr6l1SQbeDw= 

# nike 
- 支付链接：iframe标签

# 耐克 

################################
# #结果
# if 购买成功:
#   -.订单数据入库
#   -.任务结束释放用户
#   -.标记跳出层级（loop1：跳出1层，loop2:跳出2层）
#   - loop = 'loop2'
#   -.跳出2层循环，结束任务
#   break

# else 购买失败:
#   - 失败原因（唯一失败原因抽签没抽中,不用除去无货尺码）
#   user_pay_number = user_pay_number+1
#   if user_pay_number = 2:
#       break #跳出一层循环，重新拉用户
##################################################

# nike新增：
- 如果生成订单，该镜像不执行该任务，结束任务




# 明天测试准备
URL ：https://www.nike.com/cn/launch/t/lebron-soldier-xi-metallic-silver
所有尺码 ：{"allSize": [["40", "40.5", "41", "42", "42.5", "43", "44", "44.5", "45", "46"], ["c2372e3e-ae11-510e-81bf-477e33ea982a", "52c00188-b2de-5281-8ba3-a4f4216369a7", "18f79562-30d5-5fce-b93c-b494cd98e885", "42e32640-b82e-51aa-9c16-bd9274ef4f51", "6899d69f-75c7-5a71-bf86-e950435b2454", "b39880f2-8384-54b3-911c-7883bb82c3db", "b1c06a62-180f-5f86-b975-3a801606b3a8", "3b926d30-1bf8-5a1b-8ca6-09abbb3a664d", "da9d64d4-04a6-5492-84b1-3cb7fb0802ac", "331966f7-e098-5074-9ce3-25950b12ecb3"]]}



# 9-5

{"allSize": [["38.5", "39", "40", "41", "42", "42.5", "43", "44", "45", "46"], ["94589adf-d40c-599a-b91e-d5f27ddb542e", "409bb2a9-3575-508f-9edd-1b7cd54953f0", "3a9602af-69d9-5b3d-92f6-1f052cb3daba", "9b5e0d6f-c844-58e0-aaf4-5d561db92964", "153f304c-7879-5c8a-8d4d-ef3273a3ebd9", "6ca85c1f-51cb-5164-ad94-ef9dcb97752c", "1b0ae991-abd0-556f-88c8-1a427aec7836", "f4d1ca9e-c556-5a46-a94c-eb91174a928e", "9fe15fad-57fb-5c1f-a7f7-c128951f3e79", "213ee9ba-033d-517f-bfd3-68aa52e409b2"]]}


# 9-7
https://www.nike.com/cn/launch/t/air-zoom-generation-white-varsity-crimson-black

{"allSize": [["38.5", "39", "40", "41", "42", "42.5", "43", "44", "45", "46"], ["94589adf-d40c-599a-b91e-d5f27ddb542e", "409bb2a9-3575-508f-9edd-1b7cd54953f0", "3a9602af-69d9-5b3d-92f6-1f052cb3daba", "9b5e0d6f-c844-58e0-aaf4-5d561db92964", "153f304c-7879-5c8a-8d4d-ef3273a3ebd9", "6ca85c1f-51cb-5164-ad94-ef9dcb97752c", "1b0ae991-abd0-556f-88c8-1a427aec7836", "f4d1ca9e-c556-5a46-a94c-eb91174a928e", "9fe15fad-57fb-5c1f-a7f7-c128951f3e79", "213ee9ba-033d-517f-bfd3-68aa52e409b2"]]}



https://www.nike.com/cn/launch/t/womens-air-max-97-swarovski-2017

{"allSize": [["36.5", "37.5", "38", "38.5", "39", "40"], ["fd9e204e-4faf-541a-9b17-eba72d24cd14", "cf2b232c-6f12-5e77-8797-eb08cf848689", "b48f4f3d-5bf0-5806-b53a-92454035752a", "8169514f-2633-5919-a3be-7721aeecabd3", "2e9ca7f0-de33-53a5-bdf3-8bd6de5a7367", "50d75d50-8b7f-57a7-9123-243ae55c9058"]]}


{"U":"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36","L":"zh-CN","Z":"GMT+08:00","V":"1.1","F":"VOa44j1e3NlY5BSo9z4ofjb75PaK4Vpjt.gEngMQEjZrVglE4Ww.GEFF0Yz3ccbbJYMLgiPFU77qZoOSix5ezdstlYysrhsui6.Whtd9KKaKEhO3f9p_nH1u_eH3BhxUC550ialT0iakA2zGUMnGWFfwMHDCQyFA2wv4qnvtCsABIlNU.3Io3.Nzl998tp7ppfAaZ6m1CdC5MQjGejuTDRNziCvTDfWk7QTPH_KO_IvLG9mhORoVijvw2WwjftcktJJIqovE9XXTneNufuyPBDjaY2ftckuyPB884akHGOg4C15JoEJEQgc4yhpAI6.D_yg1wWF9kmFxTnx9MsFrAqJkL6f1BSnHrk0ugN.xL438IilojpVMZBHgBZEJgAxJAwEPuVrAqJkL3A237lY5BSp.5BNlan0Os5Apw.Di5"}


# 问题点
## 'code': u'PRODUCT_NOT_BUYABLE'商品不可卖要结束任务

The request failed and the request was repeated
{u'status': u'COMPLETED', u'resourceType': u'job', u'id': u'8f1f4c69-ca83-4557-9752-871b2da42a5d', u'links': {u'self': {u'ref': u'/buy/checkout_previews/v2/jobs/8f1f4c69-ca83-4557-9752-871b2da42a5d'}, u'result': {u'ref': u'/buy/checkout_preview_results/v2/8f1f4c69-ca83-4557-9752-871b2da42a5d'}}, u'error': {u'message': u'Non buyable product(s)', u'errors': [{u'field': u'request.items[0].skuId', u'message': u'skuId=f4d1ca9e-c556-5a46-a94c-eb91174a928e, productId=fb0e063a-5e4d-52d0-a810-cc732e72489b, status=HOLD, startDate=2019-09-07T01:00Z[UTC], endDate=null, availability=true, publishType=LAUNCH', u'code': u'PRODUCT_NOT_BUYABLE'}], u'httpStatus': 400}}



{"allSize": [["39", "40", "40.5", "41", "42", "42.5", "43", "44", "44.5", "45", "46"], ["f57e4bd6-cd1b-5682-a6e2-69d7e18df2e3", "e261aedf-4111-50c6-b6e5-7c5ab855849f", "32ab1a75-3e42-5f8a-bac2-7fe56a20b5fb", "699e45c9-5c7e-5f86-97ee-df58ad00f691", "6c430504-66c5-5586-94ef-3df77a53be29", "336154bb-ae62-5204-a6a4-0d35899bc94f", "a6e422f8-fd4b-5b39-848f-160344ac0c80", "2ba3228a-1d99-5f95-a606-b26a20e2f34a", "e3cb2766-2d3b-57ce-9df5-1ad2252f4c3e", "a286fab1-2724-515f-aaf5-d6691bc5dfe5", "70839bba-3964-5c2e-b07b-6b0f2cfe738f"]]}



{"allSize": [["40", "40.5", "41", "42", "42.5", "43", "44", "44.5"], ["1dfb6c0a-9b28-54f2-bb43-467f5d56424c", "86435b81-2720-5111-8667-cbf1e8aa132b", "13170512-9efc-5ca1-a62a-d8d7f04ff6f1", "c639f366-bf9d-58be-bd77-11852472d2f1", "a4f1af0e-0a7c-5d90-9d71-5279e1696baa", "6a23226f-203a-5069-aeca-7d260831918b", "296be26d-08da-5c19-a5c2-ce47a364ffc3", "9c843294-f15d-5e23-a50c-352242f41970"]]}


# 调试问题
- 部分失败原因，无法查看
- 服务器外网没有
- 循环排队循环被请求被封IP
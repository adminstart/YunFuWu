
var loadsconrent;//点菜单加载
var loadsisstatus=0;//内容是否加载完成

//申明组件
var element;
var layer;
var util;
var $;
var form;

layui.use(['element', 'layer','jquery','form'], function() {
    //加载layui组件
    element = layui.element();
    form = layui.form();
    $=layui.jquery;
    layer = layui.layer;

    element.on('nav(side)', function (elem) {
        var title   = elem.text();                              // 导航栏text
        var src     = elem.children('a').attr('data-url');      // 导航栏跳转URL
        var id      = new Date().getTime();                     // ID
        var card    = 'admin-tab';                              // 选项卡对象
        if(loadsisstatus==0){
            resultid=addtabs(title,src,id);
            //如果返回id相等证明是新加的，不相等证明是已存在
            if(resultid!=id){
                element.tabChange(card, resultid);// 切换相应的ID tab
                $('#admin-tab-container .layui-show iframe').attr('src', $('#admin-tab-container .layui-show iframe').attr('src'));
                id=resultid;
            }else{
                element.tabChange(card, id);// 切换相应的ID tab
            }
            // 提示信息
            layer.msg(title);
            // 关闭弹窗
            layer.closeAll();
            element.init();
            loadsconrent=layer.load(1, {shade: [0.8, '#393D49'],time: 1000,area: ['100px','100px']});
            loadsisstatus=id;
            var iframe = document.getElementById("iframe"+id);
            if (iframe.attachEvent) {
                iframe.attachEvent("onload", function() {
                    loadsisstatus=0;
                    layer.close(loadsconrent);
                });
            } else {
                iframe.onload = function() {
                    //iframe加载完成后你需要进行的操作
                    loadsisstatus=0;
                    layer.close(loadsconrent);
                };
            }
            $('#admin-tab li').eq(0).find("i").eq(1).remove();//初始化element之后要删除首页上面的删除图标
        }
    });

    //layer.load(1, {shade: [0.8, '#393D49'],time: 1000,area: ['100px','100px']});

    //新添加一个tabs菜单
    function addtabs(title,src,id){
        var card    = 'admin-tab';                              // 选项卡对象
        var flag    = getTitleId(card, title);                  // 是否有该选项卡存在
        // 大于0就是有该选项卡了
        if(flag > 0){
            id = flag;
        }else{
            if(src.split('/').length>2){
            content='<iframe id="iframe'+id+'" src="/' + src + '" frameborder="0"></iframe>';
            }else{
                content='<iframe id="iframe'+id+'" src="' + src + '" frameborder="0"></iframe>';
            }
            //新增
            element.tabAdd(card, {
                title: '<span>'+title+'</span>'
                , content:content
                , id: id
            });
            $(window).resize();
        }
        return id;
    }

    var is_oneloadindex=0;
    //顶部菜单点击处理
    result={};
    //这里ajax根据id查询权限表，返回result接收当前id对应的权限
    //当前id有dateurl下面没有子菜单，就直接新开一个iframe
    var url=lefturl;
    $.post(url,  function(result){
        if(result ==3004){
            layer.msg("系统错误请联系客服", {icon: 5,time: 2000});
        }else if(result ==4001){
            layer.msg("未获取系统授权", {icon: 5,time: 2000});
        }else if(result ==4004){
            layer.msg("该栏目还未公测，敬请期待。。。", {icon: 5,time: 2000});
        } else{
            if(result.length==0 && is_oneloadindex>0){
                layer.msg("该栏目还未公测，敬请期待。。。", {icon: 5,time: 2000});
            }else {

                layer.load(1, {shade: [0.8, '#393D49'],time: 1000,area: ['100px','100px']});
                //有左侧菜单
                var html='';
                $.each(result,function(i,n){
                    if(Object.keys(result[i][result[i]['act_id']]).length==0 || result[i]['act_action']){
                        html += '<li class="layui-nav-item"><a data-url="' + result[i]['act_action'] + '" href="javascript:;">';
                        html += '<i class="iconfont">'+result[i]['act_icon']+'</i><cite>'+result[i]['act_name']+'</cite></a></li>'
                    }else {
                        html += '<li class="layui-nav-item"><a href="javascript:;"><i class="layui-icon">'+result[i]['act_icon']+'</i>' + result[i]['act_name'] + '<span class="layui-nav-more"></span></a><dl class="layui-nav-child">';
                        $.each(result[i][result[i]['act_id']], function (j, n) {
                            html += '<dd><a href="javascript:;" data-url="' + result[i][result[i]['act_id']][j]['act_action'] + '">';
                            html += '<i class="layui-icon" style="width: 14px;">'+result[i][result[i]['act_id']][j]['act_icon']+'</i><cite>　' + result[i][result[i]['act_id']][j]['act_name'] + '</cite>'
                            html += '</a></dd>';
                        });
                        html += '</dl></li>';
                    }
                });
                $('#nav_menu').html(html);

                $('#nav_menu li').eq(0).addClass("layui-nav-itemed");

                if(is_oneloadindex>0) {
                    //有左边菜单，默认打开第一个
                    var oneleftcate;
                    if ($("#nav_menu li").eq(0).find("dl").length > 0) {
                        $("#nav_menu li").eq(0).addClass('layui-nav-itemed');
                        oneleftcate = $("#nav_menu li").eq(0).find("dd").eq(0);
                    } else {
                        oneleftcate = $("#nav_menu li").eq(0);
                    }

                    oneleftcate.addClass("layui-this");
                    var src = oneleftcate.children('a').attr('data-url');
                    if(src) {
                        var title = oneleftcate.children('a').find("cite").text();
                        var card = 'admin-tab';                              // 选项卡对象
                        var id = new Date().getTime();                      // ID
                        id = addtabs(title, src, id);
                        // 切换相应的ID tab
                        element.tabChange(card, id);
                        element.init();
                        // 提示信息
                        layer.msg(title);
                        // 关闭弹窗
                        layer.closeAll();
                    }else{
                        layer.msg("该栏目还未公测，敬请期待。。。", {icon: 5,time: 2000});
                    }
                }
                element.init();
                $('#admin-tab li').eq(0).find("i").eq(1).remove();
                is_oneloadindex=1;
            }
        }
    });

    /************************锁屏******************************/
    var local = [];

    function startTimer() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = m < 10 ? '0' + m : m;
        s = s < 10 ? '0' + s : s;
        $('#time').html(h + ":" + m + ":" + s);
        t = setTimeout(function() {
            startTimer()
        }, 500)
    }
    function checkLockStatus(locked) {
        if (locked == 1) {
            $('.lock-screen').show();
            $('#locker').show();
            $('#layui_layout').hide();
            $('#lock_password').val('')
        } else {
            $('.lock-screen').hide();
            $('#locker').hide();
            $('#layui_layout').show()
        }
    }
 //console.log(layui.data('lockScreen'));
    $('#lock').mouseover(function() {
        layer.tips('点击锁屏，按Alt+L可快速锁屏！', '#lock', {
            tips: [1, '#FF5722'],
            time: 2000
        })
    });

    $(document).keydown(function(e) {
        if (e.altKey && e.which == 76) {
            layer.prompt({
                formType: 1,
                value: '',
                title: '请输入锁屏密码',
                area: ['30%', '25%'],
                btnAlign:'c'
            }, function(value, index, elem){
                layui.data('lockScreen', {
                    key: 'pwd'
                    ,value: value
                });
                layer.close(index);
                lockSystem()
            });
        }
    });

    //checkLockStatus('0');

    function lockSystem() {
        local = layui.data('lockScreen');
        if(local == null)
        {
            layer.alert('锁屏失败，请稍后再试！')
        }else{
            checkLockStatus(1)
        }
        startTimer()
    }

    function unlockSystem() {
        checkLockStatus(0)
    }

    $('#lock').click(function() {
        layer.prompt({
            formType: 1,
            value: '',
            title: '请输入锁屏密码',
            area: ['30%', '25%'],
            btnAlign:'c'
        }, function(value, index, elem){
            layui.data('lockScreen', {
                key: 'pwd'
                ,value: value
            });
            layer.close(index);
            lockSystem()
        });
    });

    if(layui.data('lockScreen').pwd){
        checkLockStatus(1)
        startTimer()
    }
    form.on('submit(unlock)', function(data){
        local = layui.data('lockScreen');
        if(local != null)
        {
            if(local.pwd == data.field.lock_password)
            {
                layui.data('lockScreen', {
                    key: 'pwd'
                    ,remove: true
                });
                unlockSystem();
            }else{
                layer.alert('密码不正确！')
                return false
            }
        }else{
            layer.alert('请刷新！')
        }
    });

    $('#lock_password').keypress(function(e) {
        var key = e.which;
        if (key == 13) {
            unlockSystem()
        }
    });
    /******************************************************/

});

//iframe自适应
$(window).on('resize', function() {
    var $content = $('.admin-nav-card .layui-tab-content');
    $content.height($(this).height() - 147);
    $content.find('iframe').each(function() {
        $(this).height($content.height());
    });
}).resize();


//主页刷新
$('.layui-tab-reload').on('click',function(){
    element.tabChange('admin-tab', 0);
    $('#admin-tab-container .layui-show iframe').attr('src', $('#admin-tab-container .layui-show iframe').attr('src'));
});

// 根据导航栏text获取lay-id
function getTitleId(card,title){
    var id = -1;
    $(document).find(".layui-tab[lay-filter=" + card + "] ul li").each(function(){
        if(title === $(this).find('span').text()){
            id = $(this).attr('lay-id');
        }
    });
    return id;
}

//手机设备的简单适配
var treeMobile = $('.site-tree-mobile'),
    shadeMobile = $('.site-mobile-shade');
treeMobile.on('click', function() {
    $('body').addClass('site-mobile');
});
shadeMobile.on('click', function() {
    $('body').removeClass('site-mobile');
});



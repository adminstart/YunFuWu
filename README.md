﻿##接口调取说明：占时未用rpc所以只用简单验证机制
* 4001失败或者数据为空
* 4003 非ajax请求
* 4004 浏览器标示不为FLASHSALE

##2017-08-30
###计划任务接口
####url:/Rpc/planTask
parame:type: 1：注册  2：抢购 tid 团队标识
return：array('code' => 1, 'msg' => "成功", 'data' => resultdata);

###注册用户数据接口
####url:/Rpc/redisteruser
		parame:tid:团队id; stid:站点id
		return :array('code' => 1'msg' => "成功", 'data' => resultdata);
	
###任务用户信息接口efect:一次返回一个用户，调用之后之后释放才能其他服务器调用 
####url:/Rpc/taskuser
		parame:taskrelid:任务id; tid:团队id
		return :array('code' => 1'msg' => "成功", 'data' => resultdata);
	
	
###获取任务状态
####url:/Rpc/getTaskStatus
		parame:taskrelid:任务id; tid:团队id
		return array('code' => 1, 'msg' => "成功", 'data' => resultdata));

###记录订单
####url:/Rpc/createSignOrder
		parame:data:订单数据,uid:用户id,taskid:任务id,type:订单来源(例如1:阿迪达斯，2:nike)

			data:	订单编号 OK
				支付链接 Ok
				规格 Ok
				数量 OK 
				商品名称 OK
				单价 OK 
				优惠金额 Ok 
				快递费 OK 
				支付金额 Ok 
		return :array('code' => 1'msg' => "成功", 'data' => resultdata);
##2017-08-31
###任务用户释放接口
####url:/Rpc/releaseUser
		parame:stid:站点id,uid:用户uid
		return :array('code' => 1'msg' => "成功", 'data' => data);
		
##2017-09-14
###新的获取用户注册任务
####url:/Rpc/getRegisterTask
        parame:tid:团队id,type:任务类别，注册1
        return :array('code' => 1'msg' => "成功", 'data' => data);

###新的用户注册任务回调接口
####url:/Rpc/getRegisterTaskBack
        parame:tid:团队id,taskrelid:任务id
        return :array('code' => 1'msg' => "成功", 'data' =>data);

###新的拉取用户注册数据
####url:/Rpc/getRegisterTaskUser
        parame:tid:团队id,siteid:站点id
        return :array('code' => 1'msg' => "成功", 'data' =>data);
        
###新的拉取用户注册回调数据
####url:/Rpc/getRegisterTaskUserBack
        parame:tid:团队id,uid:用户id,siteid:站点id,status：注册状态0失败1：成功
        return :array('code' => 1'msg' => "成功", 'data' =>data);
###2017-9-29   
###获取未开始的抢购商品任务
####url:/Rpc/startTaskRel
        parame:tid:团队id
        return :array('code' => 1'msg' => "成功", 'data' =>data);
        
        
###根据时间匹配返回符合条件的定时任务数据
####url:/Rpc/timeTaskMatching
        parame:tasktime:时间（2017-09-29 18:56:01），tasktype：任务类型（修改状态定时任务传1，开主机定时任务传2）
        return :array('code' => 1'msg' => "成功", 'data' =>data);
        
###创建主机接口
####url:/Rpc/createHost
        parame:task_relid:任务id，tasktype：任务类型（修改状态定时任务传1，开主机定时任务传2）
        return :array('code' => 1'msg' => "成功", 'data' =>data);
    
###用户标注失效
####url:/Rpc/account_LoseEfficacy
        parame:uid:用户id，siteid：站点id
        return :array('code' => 1'msg' => "已标注失效", 'data' =>'');
        
###获取已经锁定的用户
####url:/Rpc/taskuser_bank
        parame:taskrelid:任务id，tid：团队id
        return :array('code' => 1'msg' => "", 'data' =>'');
